angular.module('summary', ['common'])
  .run(['$filter','$q','$rootScope','LookupSet','Map','Nav','Position','Profile'
        ,'Request','supersonic','User', function($filter,$q,$rootScope,LookupSet
        ,Map,Nav,Position,Profile,Request,supersonic,User) {
    var thisView = Nav.parseViewName(steroids.view.location);

    angular.extend($rootScope, {
      qCurrentGeoPoint: $q.defer(),
      qGeoPoint: $q.defer(),
      qSites: $q.defer(),
      qRequests: $q.defer()
    });

    Nav.initPreloadView("details");
    Nav.initPreloadView("modal");

    // obtain Lookup Sets
    LookupSet.reloadAllSets().on("value", function(snapshot) {
      var lookupSets = snapshot.val();
      var lookupSetKeys = _.keys(lookupSets);
      var lookupSetCount = lookupSetKeys.length;
      
      LookupSet._setLookupQty(lookupSetCount);
      
      _.forIn(lookupSetKeys, function(setName, index) {
        LookupSet._setLookupSet(setName, lookupSets[setName]);
      })

      $rootScope.qLookupSets.resolve(lookupSets);
    }, function(err) {
      qLookups.reject(err);
    });

    $rootScope.setupSites = function(orgTypeId, employerId) {
      //process all related sites
      Profile.setupRequiredSites(orgTypeId, employerId).then(function() {
        $rootScope.myEmployerId = Profile._getParam("myEmployerId");
        $rootScope.qSites.resolve();
      });
    }

    var buttons = ["map", "menu"];
    var subViews = ['main','icons'];
    var preloadViews = ["modal", "details"];

    Nav.initSubViews(subViews);
    Nav.switchSubView('main');
    Nav.setButtons(buttons);

    buttons[0] = Nav.initButtons('map', "map2.png", "left", 1, Nav.setupButton);    
    buttons[1] = Nav.initButtons('menu', "menu4.png", "right", 1, Nav.setupButton);
    
    steroids.view.navigationBar.show({
      title: "Requests"
    });
  }])
  .controller('SummaryCtrl', ['$window','$cordovaActionSheet','$cordovaToast'
                          ,'$cordovaDatePicker','$filter','$firebase','$mdSidenav','$q','$rootScope','$scope'
                          ,'$timeout','DB','Host','LookupSet','Map','Nav'
                          ,'ORGTYPE_CONST','Position','Profile','Request'
                          ,'supersonic','User',function ($window,$cordovaActionSheet
                          ,$cordovaToast,$cordovaDatePicker,$filter,$firebase,$mdSidenav,$q,$rootScope
                          ,$scope,$timeout,DB,Host,LookupSet,Map,Nav,ORGTYPE_CONST
                          ,Position,Profile,Request,supersonic,User) {
    // supersonic.device.ready.then(function() {
    //   $cordovaProgress.showSimple(true);
    // });

    var buttons = Nav.getButtons();
    var mapBtn = Nav.getButton("map");
    var menuBtn = Nav.getButton("menu");
    var qModalStarted = $q.defer();
    var subViews = Nav._getSubViews();
    var thisView = Nav.parseViewName(steroids.view.location);
    var userInfo;

    mapBtn.navBtn.onTap = function() {
      /* check whether requests have been updated (requests will be updated whenever
      new filter has been selected || if a new requests is brought into the system.
      Bind listener to any request updates  */
      
      Nav.toggleSubView(subViews);
      $scope.$digest();
    }

    menuBtn.navBtn.onTap = function() {
      $mdSidenav('left').toggle().then(function(){
        console.log("side nav closed");
      });

      $scope.$apply();
    }

    angular.extend($scope, {
      clients: null,
      constituentSites: null,
      distances: null,
      employerImgFilepath: "",
      employerSite: null,
      employerSites: null,
      flashMsg: [],
      requests: null,
      submitRequest: null,
      subViews: Nav._getSubViews(),
      test: null,
      themeColor: null
    });

    var options = { enableHighAccuracy: false };
    var unwatch = supersonic.device.geolocation.watchPosition(options).onValue(function(position) {
      var coords = position.coords;
      var sender = Nav.parseViewName(steroids.view.location);      
      var newGeoPoint = {
        lat: coords.latitude,
        long: coords.longitude,
        latitude: coords.latitude,
        longitude: coords.longitude
      };

      $rootScope.qRequests.promise.then(function(requests) {
        var distanceDiff = 1000 * Position.calcDistance($rootScope.currentGeoPoint, newGeoPoint);  
        
        if (distanceDiff > 1) {
          Position._setGeoPoint($rootScope.currentGeoPoint = newGeoPoint);
          var locationParams = {
            sender: sender,
            content: { geoPoint: newGeoPoint }
          };

          supersonic.data.channel("locationData").publish(locationParams);
          Position.prepForDistances(requests, newGeoPoint).then(function(resp) {
            $scope.distances = resp.rows[0].elements;
          }, function(errMessage) {
            console.log(errMessage);
          });
        }
      });
    });

    $scope.$watch("requests.length", function(newLength, oldCnt) {
      if (newLength > 0) {
        var currentGeoPoint = Position.getGeoPoint();

        Position.prepForDistances($scope.requests, currentGeoPoint).then(function(resp) {
          $scope.distances = resp.rows[0].elements;
        }, function(errMessage) {
          console.log(errMessage);
        });
      }
    });

    $q.all([$rootScope.qUserInfo.promise,$rootScope.qLookupSets.promise])
      .then(function(dependencies) {
        userInfo = $scope.userInfo = dependencies[0];
        
        /* prepping setupSites() is unique to Summary view, as no core data is published
        from Login view to Summary view */
        $scope.prepSetupSites(dependencies[0]);

        Profile.setupRequiredSites($rootScope.myOrgTypeId).then(function(constituentSites) {
          $rootScope.myEmployerId = Profile._getParam("myEmployerId");
          var employerOrgTypeName = Profile.getOrgTypeName($rootScope.myOrgTypeId);
          var lookupSets = LookupSet._getAllLookupSets();

          $scope.constituentSites = Profile._getSites(Profile.toggleOrgTypeId($rootScope.myOrgTypeId));
          $scope.employerSites = Profile._getSites($rootScope.myOrgTypeId);
          $scope.employerSite = _.findWhere( $scope.employerSites, {'$id': $rootScope.myEmployerId} );
          $scope.subViews = Nav._getSubViews();
        });
      });

    $scope.openDetails = function(request) {
      var constituentOrgTypeId = Profile.toggleOrgTypeId( Profile._getParam("myOrgTypeId") );
      var constituentOrgTypeName = $filter('orgType')(constituentOrgTypeId, "name");
      
      var options = Nav.buildOnTapOptions("detailsData","pillar",supersonic.ui.layers.push,{
        reqId: request.$id,
        constituentSiteId: request[constituentOrgTypeName].id
      });

      Nav.enterView("details", options);
    }

    $scope.test = function() {
      var options = {
        title: 'What do you want with this image?',
        buttonLabels: ['Share via Facebook', 'Share via Twitter'],
        addCancelButtonWithLabel: 'Cancel',
        androidEnableCancelButton : true,
        winphoneEnableCancelButton : true,
        addDestructiveButtonWithLabel : 'Delete it'
      };

        $cordovaActionSheet.show(options).then(function(btnIndex) {
          var index = btnIndex;
        });
    }
    
    // set EmployerId && chosenOrgTypeId && sites (employers + constituents)
    $scope.prepSetupSites = function(userInfo) {
      if (!$rootScope.myEmployerId || $rootScope.myEmployerId != ""
          || !$rootScope.myOrgTypeId || $rootScope.myOrgTypeId != "") {
        Profile.retrieveCoreIds(userInfo);
        $rootScope.qMyOrgTypeId.resolve();
      }
      
      $rootScope.setupSites($rootScope.myOrgTypeId, $rootScope.myEmployerId);
    }

    // get users from selected employer
    $rootScope.qSites.promise.then(function() {
      Nav.startView("details");
      var chosenEmployerSites = Profile._getSites($rootScope.myOrgTypeId);
      var chosenEmployerSite = _.find(chosenEmployerSites, {
        $id: $rootScope.myEmployerId
      });

      var requests = Request._getRequests();

      // get requests
      if (!requests || requests.length > 0)
        $rootScope.qRequests.resolve(requests);
      else {
        Request.setupRequests($rootScope.myOrgTypeId).then(function(fbReqs) {
          $scope.requests = fbReqs;
          $rootScope.qRequests.resolve(fbReqs);
          $timeout(function() {
            Nav.startView("modal");
            qModalStarted.resolve();
          }, 800);

          // $timeout(function() {
          //   $cordovaProgress.hide();
          // }, 9800);

          var options = {
            date: new Date(),
            mode: 'date', // or 'time'
            minDate: new Date() - 10000,
            allowOldDates: true,
            allowFutureDates: false,
            doneButtonLabel: 'DONE',
            doneButtonColor: '#F2F3F4',
            cancelButtonLabel: 'CANCEL',
            cancelButtonColor: '#000000'
          };
        });
      }
    });

    $rootScope.qMyOrgTypeId.promise.then(function() {
      var buttons = Nav.getButtons();
      $scope.themeColor = Profile._getParam("colors")[$rootScope.myOrgTypeId];

      if ($rootScope.myOrgTypeId == ORGTYPE_CONST.CLIENT) {
        var createBtn = Nav.initButtons('create', "add3.png", "right", 0, Nav.setupButton);
        Nav.setButton(createBtn);
        createBtn.navBtn.onTap = function() {
          Nav.enterView("modal", Nav.modalOnTapOptions("create", supersonic.ui.modal.show));
        }
      }
    });

    qModalStarted.promise.then(function() {
      steroids.view.navigationBar.update({
        styleClass: "super-navbar",
        overrideBackButton: true,
        buttons: {
          left: _.pluck(_.where(buttons, {side: "left"}), "navBtn"),
          right: _.pluck(_.where(buttons, {side: "right"}), "navBtn")
        }
      });
    });
  }]);