angular.module('modal', ['common', 'ui.bootstrap'])
  .run(['$q','$rootScope','$templateCache','Nav','supersonic',function($q,$rootScope
      ,$templateCache,Nav,supersonic) {
    var thisView = Nav.parseViewName(steroids.view.location); 
    supersonic.device.ready.then( function() {
      console.log("Modal ready");
      supersonic.data.channel("DOMReady").publish();
    });

    $templateCache.put("promptToSave.html",
      '<md-bottom-sheet class="md-grid">'
        +'<md-subheader>Work will be lost. Close anyway?</md-subheader>'
        +'<md-list>'
          +'<md-item>'
            +'<md-button class="md-grid-item-content" aria-label="Yes" ng-click="giveAnswer(true)">'
              +'<p class="md-grid-text">YES</p>'
            +'</md-button>'
          +'</md-item>'
          +'<md-item>'
            +'<md-button class="md-grid-item-content" aria-label="No" ng-click="giveAnswer(false)">'
              +'<p class="md-grid-text">NO</p>'
            +'</md-button>'
          +'</md-item>'
        +'</md-list>'
      +'</md-bottom-sheet>');
  }])
  .controller('ModalCtrl', ['$mdBottomSheet','$q','$rootScope','$scope','Nav'
                        ,'Profile','supersonic','Watcher', function($mdBottomSheet
                        ,$q,$rootScope,$scope,Nav,Profile,supersonic,Watcher) {
    var thisView = Nav.thisView();
    Nav.initSubViews(["create", "profile", "map", "setStatus"]);
    angular.extend($scope, {
      mySites: null,
      startMainWatcher: null,
      subViews: Nav._getSubViews()
    });

    // var buttons = new Array("close");
    // Nav.setButtons(buttons);

    // buttons[0] = Nav.initButtons('close', "close.png", "right", 1, Nav.setupButton);
    // buttons[0].navBtn.onTap = function() {
    //   if ($scope.promptToSave == true) {
    //     var btmSheetOptions = {
    //       controller: "btmSheetCtrl",
    //       templateUrl: "promptToSave.html"
    //     }
    //     $mdBottomSheet.show(btmSheetOptions).then(function() {
    //       // continue to close
    //       Nav.resetSubViews();
    //       Nav.exitView(thisView, supersonic.ui.modal.hide);
    //       $scope.$digest();  
    //     }, function() {
    //       // do not close
    //       return;
    //     });
    //   } else {
    //     Nav.resetSubViews();
    //     Nav.exitView(thisView, supersonic.ui.modal.hide);
    //     $scope.$digest();  
    //   }
    // }

    /****************************** ERASE LATER ******************************
    **************************************************************************
    *************************************************************************/
    // Nav.switchSubView("profile");
    /****************************** ERASE LATER ******************************
    **************************************************************************
    *************************************************************************/
    function _startMainWatcher() {
      var stopMainWatcher = supersonic.data.channel(thisView +"Data").subscribe( function(message) {
        var targetSubView = message.content.targetSubView;
        _startGeoWatcher(message);
        Watcher.addToWatchers(_startMainWatcher);
        stopMainWatcher();

        $rootScope.qUserInfo.promise.then(function(userInfo) {
          var accessRights = Profile.findActiveAccessRights(userInfo);
          var employerSiteIds = _.pluck( _.where(accessRights, {orgTypeId: $rootScope.myOrgTypeId}), "siteId");
          var qMySites = $q.defer();
          
          // check old siteIds == new siteids so DB call does not have to be made
          var oldSites = Profile._getSites($rootScope.myOrgTypeId);
          var oldSiteIds = _.pluck(oldSites, "$id");
          var missingSiteIds = _.difference(employerSiteIds, oldSiteIds);

          if (missingSiteIds.length == 0)
            qMySites.resolve(oldSites);
          else {
            // retrieve employer sites
            var mySites = [];

            _.forIn(employerSiteIds, function(siteId, index) {
              var employerTypeName = Profile.getOrgTypeName($rootScope.myOrgTypeId);
              Profile.retrieveSite(siteId, employerTypeName).then(function(employerSite) {
                mySites.push(employerSite);
                
                if (index == (employerSiteIds.length - 1)) {
                  Profile._setSites($rootScope.myOrgTypeId, mySites);
                  qMySites.resolve(mySites);
                }
              });
            });
          }

          qMySites.promise.then(function(sites) {
            $scope.mySites = sites;
          });
        });

        if ($scope.subViews[targetSubView] != true)
          Nav.switchSubView(targetSubView);

        var readyParams = {
          sender: Nav.parseViewName(steroids.view.location),
          content: {}
        };

        supersonic.data.channel(thisView +"Ready").publish(readyParams);
        steroids.view.navigationBar.show({
          title: ""
        });

        $scope.$digest();
      });
    }

    function _startGeoWatcher(message) {
      var stopGeoWatcher = supersonic.data.channel("locationData").subscribe( function(message) {
        Position._setGeoPoint($rootScope.currentGeoPoint = message.content.geoPoint);
        $rootScope.$digest();
      });

      Watcher._addToUnwatchers(stopGeoWatcher);
    }

    _startMainWatcher();
  }])
  .directive('create', function() {
    return {
      restrict: "EA",
      scope: {
        mySites: "="
      },
      template:
        // '<div>'
        // +'  <button ng-click="test(2)">Vehicle</button>'
        // +'  <button ng-click="test(1)">License Plate</button>'
        // +'</div>'
        // +'<div>{{flashMsg}}</div>'
        '<div class="row item-image">'
        +'  <span class="col col-5"></span>'

        // <!-- P I C S   I N D I C A T O R-->
        +'  <span class="col col-20">'
        +'    <i ng-show="widgets[\'pics\']" ng-class="{\'icon button-icon super-ios7-checkmark balanced\':requirements.pics,'
        +'                                              \'icon button-icon super-ios7-circle-filled\':!requirements.pics}"></i>'
        +'    <i ng-show="!widgets[\'pics\']" ng-class="{\'icon button-icon super-ios7-checkmark-outline balanced\': requirements.pics,'
        +'                                               \'icon button-icon super-ios7-circle-outline\': !requirements.pics}"'
        +'                                    ng-click="switchWidget(\'pics\')"></i>'
        +'    <p>What</p>'
        +'  </span>'
        +'  <span class="col col-5"></span>'

        // <!-- S I T E    I N D I C A T O R-->
        +'  <span class="col col-20">'
        +'    <i ng-show="widgets[\'site\']" ng-class="{\'icon button-icon super-ios7-checkmark balanced\':requirements.site,'
        +'                                              \'icon button-icon super-ios7-circle-filled\':!requirements.site}"></i>'
        +'    <i ng-show="!widgets[\'site\']" ng-class="{\'icon button-icon super-ios7-checkmark-outline balanced\': requirements.site,'
        +'                                               \'icon button-icon super-ios7-circle-outline\': !requirements.site}"'
        +'                                    ng-click="switchWidget(\'site\')"></i>'
        +'    <p>Where</p>'
        +'  </span>'
        +'  <span class="col col-5"></span>'

          // <!-- T I M E S T A M P    I N D I C A T O R-->
        +'  <span class="col col-20">'
        +'    <i ng-show="widgets[\'datetime\']" ng-class="{\'icon button-icon super-ios7-checkmark balanced\':requirements.datetime,'
        +'                                                  \'icon button-icon super-ios7-circle-filled\':!requirements.datetime}"></i>'
        +'    <i ng-show="!widgets[\'datetime\']" ng-class="{\'icon button-icon super-ios7-checkmark-outline balanced\': requirements.datetime,'
        +'                                                   \'icon button-icon super-ios7-circle-outline\': !requirements.datetime}"'
        +'                                        ng-click="switchWidget(\'datetime\')"></i>'
        +'    <p>When</p>'
        +'  </span>'
        +'  <span class="col col-5"></span>'

          // <!-- ! F I N A L I Z E   I N D I C A T O R! -->'
        +'  <span class="col col-25">'
        +'    <i ng-show="requirements.pics && requirements.site && requirements.datetime"'
        +'       ng-class="{\'icon button-icon super-ios7-cloud-upload balanced\': widgets[\'confirm\'],'
        +'                  \'icon button-icon super-ios7-cloud-upload-outline balanced\': !widgets[\'confirm\']}"'
        +'       ng-click="switchWidget(\'confirm\')"></i>'
        +'    <i ng-show="!requirements.pics || !requirements.site || !requirements.datetime"'
        +'       class="icon button-icon super-ios7-cloud-upload outline stable"></i>'
        +'    <p ng-class="{\'stable\':!requirements.pics || !requirements.site || !requirements.datetime}">Confirm</p>'
        +'  </span>'

        +'</div>'
        +'<div pics-mgr ng-if="mySites"'
        +'              ng-show="widgets[\'pics\']"'
        +'              indicator="requirements.pics"'
        +'              pic-class="list">'
        +'</div>'
        +'<div site-picker ng-if="mySites"'
        +'                 ng-show="widgets[\'site\']"'
        +'                 class="list"'
        +'                 indicator="requirements.site"'
        +'                 request="newRequest"'
        +'                 sites="sites">'
        +'</div>'
        +'<div datetime-mgr ng-if="mySites"'
        +'                  ng-show="widgets[\'datetime\']"'
        +'                  model="newRequest.firstSeen"'
        +'                  indicator="requirements.datetime">'
        +'</div>'
        +'<div ng-show="widgets[\'confirm\']">'

        +'  <div class="list">'
        +'    <span class="item item-divider">Finalize Details</span>'
        +'  </div>'
        +'  <div class="list item">'
        +'    <p class="item-body"><b>Vehicle Pictures:</b> VIN, License Plate</p>'
        +'    <p class="item-body"><b>Location:</b>{{sites.client.name}}</p>'
        +'    <p class="item-body"><b>Tow Provider:</b>{{sites.vendor.name}}</p>'
        +'    <p class="item-body"><b>First Seen:</b>{{newRequest.firstSeen | date : "medium"}}</p>'
        +'  </div>'
        +'  <label class="list item item-input">'
        +'    <textarea class="item-note"'
        +'              rows="3"'
        +'              ng-model="notes"'
        +'              placeholder="Comments...">'
        +'    </textarea>'
        +'  </label>'

        +'  <div class="list">'
        +'    <div class="item-text-wrap item-note">By clicking either of the two buttons below, you are agreeing to the <a href="#">terms</a>.</div>'
        +'    <button class="button button-block button button-calm" ng-click="submitRequest(0)"><b>Advise To Tow</b></button>'
        +'    <button class="button button-block button button-balanced" ng-click="submitRequest(1)"><b>Tow Now!</b></button>'
        +'  </div>'
        +'</div>',
      controller: ['$firebase','$http','$q','$rootScope','$scope','Host','LookupSet'
                  ,'Nav','ORGTYPE_CONST','PICTYPE_CONST','Position','Profile','Request'
                  ,'supersonic','Watcher', function($firebase,$http,$q,$rootScope
                  ,$scope,Host,LookupSet,Nav,ORGTYPE_CONST ,PICTYPE_CONST,Position
                  ,Profile,Request,supersonic,Watcher) {
        // setup Nav stuff
        var thisView = Nav.thisView();
        var widgetNames = ['pics','site','datetime','confirm'];

        Nav.initWidgets(widgetNames);
        Nav.switchWidget("pics");

        Request._setRequiredPicTypeIds(new Array(PICTYPE_CONST.VEHICLE, PICTYPE_CONST.LICPLATE))

        angular.extend($scope, {
          flashMsg: "tseting...",
          lookupSets: null,
          notes: "",
          newRequest: {
            iid: "",
            firstSeen: moment().toDate(),
            vendors: {
              id: ""
            },
            clients: {
              id: ""
            },
            geoPoint: null,
            pics: [],
            statuses: [],
            vehicle: {
              make: "",
              model: "",
              color: {
                hexCode: "",
                name: ""
              },
              vehicleType: {
                id: "",
                name: ""
              },
              year: "",
              drivetrain: {
                id: "",
                name: ""
              },
              transmission: {
                id: "",
                name: ""
              },
              vin: "",
              licensePlate: {
                number: "",
                state: ""
              }
            }
          },
          requirements: {
            pics: false,
            site: false,
            datetime: false
          },
          sites: {
            client: null,
            vendor: null
          },
          switchWidget: null,
          subViews: Nav._getSubViews(),
          submitRequest: function(statusId) {
            var qS3Data = $q.defer();
            statusId = statusId.toString();

            $http({
              url: 'http://towimg.martiangold.com/s3-upload.php?bucket=towmo',
              method: "GET",
              data: {"bucket": "towmo"},
              withCredentials: true,
              headers: {
                'Content-Type': 'application/json; charset=utf-8'
              }
            }).success(function(data, status, headers, config) {
              qS3Data.resolve(data);
            }).error(function(data, status, headers, config) {
              navigator.notification.alert("No S3 Policy");
            });

            // 1. preparations prior to file upload
            var clientId = $scope.newRequest.clients.id;
            var fileTransfer = new FileTransfer();
            var orgTypeId = $rootScope.myOrgTypeId;
            var stagedPics = Request._getStagedPics();
            var timestamp = moment().format("X");
            var userId = Profile._getParam("userInfo").$id;
            var qStagedPics = $q.defer();

            _.forIn(stagedPics, function(stagedPic, index) {
              var fileToUpload = stagedPic.file.sourceFilename;
              var fileType = stagedPic.file.fileType;
              var fuOptions = new FileUploadOptions();
              var picTypeId = stagedPic.iid;

              stagedPic.dbRecord.filename = Request.generateImgId(timestamp,userId
                                          ,clientId,orgTypeId,picTypeId,fileType);
              stagedPic.dbRecord.statusId = statusId;
              
              qS3Data.promise.then(function(s3Data) {
                fuOptions.mimeType = "image/" +fileType;
                fuOptions.params = {
                  key: "requests/vehicle/base/" +stagedPic.dbRecord.filename,
                  acl: 'public-read',
                  'Content-Type': "image/" +stagedPic.file.fileType,
                  AWSAccessKeyId: s3Data.key,
                  policy: s3Data.policy,
                  signature: s3Data.signature
                };

                fileTransfer.upload(fileToUpload, encodeURI(s3Data.url), function(good) {
                  if ( index == (stagedPics.length - 1) )
                    qStagedPics.resolve();
                  // navigator.notification.alert("picType " +picTypeId +" uploaded");
                }, function(error) {
                  // navigator.notification.alert("Failed");
                }, fuOptions);
              });

              $scope.newRequest.pics.push(stagedPic.dbRecord);
            });

            // // 3. assign geoPoint of license plate newRequest.geoPoint
            var licPlateImg = _.findWhere($scope.newRequest.pics, {picTypeId: PICTYPE_CONST.LICPLATE});
            var statusEntry = {
              timestamp: moment( moment().toDate() ).format(),
              geoPoint: licPlateImg.geoPoint,
              statusId: statusId,
              author: {
                id: userId,
                orgTypeId: $rootScope.myOrgTypeId
              },
              notes: $scope.notes,
              signFilename: ""
            }

            $scope.newRequest.statuses.push(statusEntry);
            $scope.newRequest.geoPoint = licPlateImg.geoPoint;
            $scope.newRequest.firstSeen = moment($scope.newRequest.firstSeen).format();

            var fbReq = $firebase(Request.publishRequest($scope.newRequest).ref()).$asObject();
            fbReq.$loaded().then(function(req) {
              var status = LookupSet._getLookupSet("statuses", statusId);

              qStagedPics.promise.then(function() {
                if (status.assignToVendor == true) {
                  // add request $id to vendor's request array
                  var vendorSites = Profile._getSites(ORGTYPE_CONST.VENDOR);
                  var vendorSite = _.first(vendorSites);
                  Request.assignToSite(vendorSite, req.$id);
                }

                // add request $id to client's request array
                var clientSite = _.first( Profile._getSites($rootScope.myOrgTypeId) );
                Request.assignToSite(clientSite, req.$id);
                return;
              }).then(function() {
                // exit modal after completion
                Nav.exitView(thisView, supersonic.ui.modal.hide);
              });
            });
          },
          test: function(picTypeId) {
            var stagedPic = Request._getStagedPic(picTypeId);
            $scope.flashMsg = stagedPic.file || "NULL";
          },
          pic: function(picId) {
            var c = $scope.newRequest.pics[picId - 1].picTypeId;
            navigator.notification.alert("pic " +picId + ": " +c);
          },
          timestamp: null,
          widgets: Nav._getWidgets()
        });

        $scope.switchWidget = function(targetWidget) {
          Nav.switchWidget(targetWidget);
        }

        var closeBtn = Nav.getButton("close");
        supersonic.ui.views.current.whenVisible(function() {
          steroids.view.navigationBar.show({
            title: "New Request"
          });

          steroids.view.navigationBar.update({
            styleClass: "super-navbar",
            overrideBackButton: true,
            buttons: {
              left: [],
              right: [closeBtn.navBtn]
            }
          });
        });
      }]
    }
  })
  .directive("profile", function() {
    return {
      restrict: "EA",
      scope: true,
      template:
        // Profile picture
        '<md-content ng-show="widgets[\'home\']">'
          +'<md-list>'
        
            +'<md-item>'
              +'<md-item-content>'
                // user image || human icon
                +'<div class="md-tile-content">'
                  +'<md-icon ng-show="!userInfo.filename || userInfo.filename == \'\'"'
                             +'style="font-size:152px;height:128px;"'
                             +'md-font-icon="super-ios7-contact-outline">'
                  +'</md-icon>'
                  +'<div class="item-image face">'
                    +'<img ng-show="userInfo.filename && userInfo.filename != \'\'" ng-src="{{imgFilepath.user}}{{userInfo.filename}}" />'
                  +'</div>'
                +'</div>'
                
                // personal info
                +'<div class="md-tile-content">'
                  +'<h3>{{userInfo.name.first}} {{userInfo.name.last}}</h3>'
                  +'<h3>{{userInfo.email}}</h3>'
                  +'<h3>{{userInfo.phoneNum}}</h3>'
                +'</div>'
              +'</md-item-content>'
              +'<md-divider></md-divider>'
            +'</md-item>'
            
            // menu items
            +'<md-item ng-repeat="link in links" ng-show="!$last">'
              +'<md-item-content ng-click="switchWidget(link.widget)">'
                +'<div class="md-tile-left">'
                  +'<md-icon md-font-icon={{link.icon}} style="font-size:32px"></md-icon>'
                +'</div>'
                +'<div class="md-tile-content">'
                  +'<h3>{{link.name}}</h3>'
                +'</div>'
              +'</md-item-content>'
              +'<md-divider md-inset ng-if="!$last"></md-divider>'
            +'</md-item>'

            // logout
            +'<md-item>'
              +'<md-item-content ng-click="logout()">'
                +'<div class="md-tile-left">'
                  +'<md-icon md-font-icon={{links[links.length-1].icon}} style="font-size:32px"></md-icon>'
                +'</div>'
                +'<div class="md-tile-content">'
                  +'<h3>{{links[links.length-1].name}}</h3>'
                +'</div>'
              +'</md-item-content>'
            +'</md-item>'
          +'</md-list>'
        +'</md-content>'

        +'<div ng-repeat="link in links"'
              +'ng-show="widgets[link.widget]"'
              +'ng-click="switchWidget(\'home\')" >{{link.name}}'
        +'</div>',
      controller: ["$scope","Host","Nav","Profile", function($scope,Host,Nav,Profile) {
        var _widgetNames = ['home','info','settings','assignments','vehicle'];
        Nav.initWidgets(_widgetNames);
        Nav.switchWidget("home");

        angular.extend($scope, {
          imgFilepath: {
            user: Host.buildFilepath("users", "base"),
            vehicle: Host.buildFilepath("vehicles", "avatar")
          },
          links: [
            {
              name: "My Info",
              icon: "super-ios7-person-outline",
              action: Nav.switchWidget,
              widget: "info"
            }, {
              name: "Settings",
              icon: "super-ios7-toggle-outline",
              action: Nav.switchWidget,
              widget: "settings"
            }, {
              name: "Assignments",
              icon: "super-ios7-checkmark-outline",
              action: Nav.switchWidget,
              widget: "assignments"
            }, {
              name: "My Vehicle",
              icon: "super-model-s",
              action: Nav.switchWidget,
              widget: "vehicle"
            }, {
              name: "Logout",
              icon: "super-ios7-locked-outline",
              action: Nav.logout
            }
          ],

          userInfo: Profile._getParam("userInfo"),
          widgets: Nav._getWidgets(),
          logout: function() {
            Nav.logout();
          },
          switchWidget: function(widgetName) {
            Nav.switchWidget(widgetName);
          }
        });

        supersonic.ui.views.current.whenVisible(function() {
          steroids.view.navigationBar.show({
            title: "Profile"
          });
          steroids.view.navigationBar.update({
            styleClass: "super-navbar",
            buttons: {
              left: [],
              right: []
            }
          });
        });
      }]
    }
  })
  .directive("setStatus", function() {
    return {
      restrict: "EA",
      scope: {},
      template:
        '<next-statuses class="list"'
                      // +'ng-if="request"'
                      +'model="request.statuses"'
                      +'status-id="chosenStatusId">'
        +'</next-statuses>'
        +'<label class="list item item-input">'
          +'<textarea class="item-note"'
                      +'rows="7"'
                      +'ng-model="notes"'
                      +'ng-change="checkNotes()"'
                      +'placeholder="Add comments if you would like [optional]...">'
          +'</textarea>'
        +'</label>'

        +'<div class="list">'
          +'<button class="button button-block button button-balanced" ng-click="submit(chosenStatusId)">Update!</button>'
        +'</div>',
      controller: ['$mdBottomSheet','$rootScope','$scope','DB','Nav','LookupSet'
              ,'ORGTYPE_CONST','Position','Profile','Request',function($mdBottomSheet
              ,$rootScope,$scope,DB,Nav,LookupSet,ORGTYPE_CONST,Position,Profile
              ,Request) {
        var backBtn = Nav.initButtons('back', "back2.png", "left", 1, Nav.setupButton);
        var userInfo = Profile._getParam("userInfo");

        backBtn.navBtn.onTap = function() {
          if (!$scope.notes || $scope.notes == "") {
            Nav.leaveModal(supersonic.ui.layers.pop);
          } else {
            var btmSheetOptions = {
              controller: "btmSheetCtrl",
              templateUrl: "promptToSave.html"
            }
            $mdBottomSheet.show(btmSheetOptions).then(function() {
              // continue to close
              Nav.leaveModal(supersonic.ui.layers.pop);
            }, function() {
              // do not close
              return;
            });
          }

          $scope.$parent.subViews['setStatus'] = false;
          $scope.request = null;
          $scope.$apply();
        }
        var reqId = localStorage.getItem("reqId");
        var oldRequest = Request._getRequest();

        // function _leaveModal(navMethod) {
        //   Nav.resetSubViews();
          
        //   var thisView = Nav.parseViewName(steroids.view.location);
        //   Nav.exitView(thisView, navMethod);
        // }

        function _showNavBar() {
          steroids.view.navigationBar.show({
            title: "Update Status"
          });

          steroids.view.navigationBar.update({
            styleClass: "super-navbar",
            overrideBackButton: true,
            buttons: {
              left: [backBtn.navBtn],
              right: []
            }
          });
        }

        supersonic.ui.views.current.whenVisible(_showNavBar);

        // Request._setReqId(reqId);
        angular.extend($scope, {
          chosenStatusId: "",
          notes: null,
          promptToSave: false,
          request: null,
          submit: function(chosenStatusId) {
            // complete status update
            var data = {
              author: {
                id: userInfo.$id,
                orgTypeId: Profile._getParam("myOrgTypeId")
              },
              statusId: chosenStatusId,
              timestamp: moment().format(),
              geoPoint: Position.getGeoPoint()
            };

            var statusEntry = Request.buildChildTemplate(data);
            
            Request._setStatus(statusEntry).then(function() {
              var nextStatusLookup = LookupSet._getLookupSet("statuses", $scope.chosenStatusId);
              
              // assign statusId to vendor if tow request shifts from "suggested" -> "ready to tow"
              if (nextStatusLookup.assignTo[ORGTYPE_CONST.VENDOR].site == true) {
                var orgTypeName = Profile.getOrgTypeName(ORGTYPE_CONST.VENDOR);
                var vendorSites = Profile._getSites(ORGTYPE_CONST.VENDOR);
                var vendorSite;

                // get vendor site if none was retrieve before
                if (!vendorSites) {                  
                  DB.getDbRecord(orgTypeName +"/" +$scope.request[orgTypeName].id).then(function(foundSite) {
                    Request.assignToSite(foundSite, $scope.request.$id);
                    Profile._setSites(ORGTYPE_CONST.VENDOR, [foundSite]);
                  });
                } else {
                  vendorSite = _.findWhere(vendorSites, {$id: $scope.request[orgTypeName].id});  
                  Request.assignToSite(vendorSite, $scope.request.$id);
                }
              }

              // assign request Id to user's current State when next status has islocked: true
              if (nextStatusLookup.assignTo[ORGTYPE_CONST.VENDOR].user)
                userInfo.state.requestId = $scope.request.$id;
              else
                userInfo.state.requestId = "";
              
              userInfo.$save();
              return;
            }).then(function() {
              Nav.leaveModal(supersonic.ui.layers.pop);
            });
          }
        });

        if (angular.isDefined(oldRequest) && oldRequest.$id == reqId)
          $scope.request = oldRequest;
        else {
          Request.retrieveRequest(reqId).then(function(newRequest) {
            $scope.request = newRequest;
            Request._setRequest(newRequest);
          });  
        }

        function obtainVendor(chosenClientSite) {
          var activeVendor = _.findWhere(chosenClientSite.vendors, {isActive: true});
          var vendorOrgTypeName = Profile.getOrgTypeName(ORGTYPE_CONST.VENDOR);
          
          return Profile.retrieveSite(activeVendor.id, vendorOrgTypeName).then(function(vendorSite) {
            Profile._setSites(ORGTYPE_CONST.VENDOR, [vendorSite]);
            return vendorSite;
          });
          //set vendor in Service && newRequest.vendors.id
        }

        _showNavBar();
      }]
    };
  })
  .directive("sitePicker", function() {
    return {
      restrict: "EA",
      scope: {
        doneFlag: "=indicator",
        // mySites: "=model",
        request: "=",
        sites: "="
      },
      template:
        '<div class="list">'
        +'  <span class="item item-divider">Where Is Vehicle Parked?</span>'
        +'</div>'
        +'<div ng-repeat="site in mySites" ng-click="setSite(site)"'
        +'     ng-class="{\'item item-icon-right balanced\':sites.client.$id == site.$id,'
        +'                \'item item-icon-right\':sites.client.$id != site.$id}">'
        +'  <span ng-class="item-text-wrap">'
        +'    <h2>{{site.name}}</h2>'
        +'    {{site.address.street.number}} {{site.address.street.name}} {{site.address.street.type}}'
        +'  </span>'
        +'  <i class="icon item-avatar super-ios7-checkmark-empty" ng-show="sites.client.$id == site.$id"></i>'
        +'</div>',
      controller: ['$rootScope','$scope','ORGTYPE_CONST','Profile',function (
                  $rootScope, $scope, ORGTYPE_CONST, Profile) {
        var myOrgTypeId = Profile._getParam("myOrgTypeId");
        angular.extend($scope, {
          flashMsg: "Select Site Below",
          mySites: Profile._getSites($rootScope.myOrgTypeId),
          test: function() {
            console.log("client Site: ", $scope.sites.client);
            console.log("client Site Index: ", $scope.sites.client.$id);
            console.log("vendor Site: ", $scope.sites.vendor);
            console.log("$scope vendorId: ", $scope.request.vendors.id);
          }
        });

        $scope.setSite = function(chosenSite) {
          $scope.request.clients.id = chosenSite.$id;
          $scope.sites.client = chosenSite;
          $scope.doneFlag = $scope.sites.client ? true : false;

          var vendorSite = Profile._getSites(ORGTYPE_CONST.VENDOR)[0];
          if (!vendorSite)
            obtainVendor(chosenSite);
          else {
            $scope.request.vendors.id = vendorSite.$id;
            $scope.sites.vendor = vendorSite;
          }

        }

        function obtainVendor(chosenClientSite) {
          var activeVendor = _.findWhere(chosenClientSite.vendors, {isActive: true});
          var vendorOrgTypeName = Profile.getOrgTypeName(ORGTYPE_CONST.VENDOR);
          Profile.retrieveSite(activeVendor.id, vendorOrgTypeName).then(function(vendorSite) {
            Profile._setSites(ORGTYPE_CONST.VENDOR, [vendorSite]);
            $scope.request.vendors.id = vendorSite.$id;
            $scope.sites.vendor = vendorSite;
          });
          //set vendor in Service && newRequest.vendors.id
        }

        var chosenSite = _.first($scope.mySites);
        $scope.setSite(chosenSite);
      }]
    }
  })