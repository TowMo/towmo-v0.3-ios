angular.module('details', ['common'])
  .run(['$filter','$q','$rootScope','$templateCache','LookupSet','Map','Nav'
        ,'Position','Profile','Request','supersonic','User', function($filter,$q
        ,$rootScope,$templateCache,LookupSet,Map,Nav,Position,Profile,Request
        ,supersonic,User) {
    supersonic.device.ready.then( function() {
      console.log("Details ready");
      supersonic.data.channel("DOMReady").publish();
    });

    // angular.extend($rootScope, {
    //   qRequest: $q.defer()
    // });

    $templateCache.put("vehicle-info-clients.html",
      '<div class="list list-inset">'
      +'  <label class="item item-input">'
      +'    <span class="input-label">VIN: </span>'
      +'    <div>{{vCopy.vin}}</div>'
      +'  </label>'
      +'  <label class="item item-input">'
      +'    <span class="input-label">License Plate #</span>'
      +'    <div>{{vCopy.licensePlate.number}}</div>'
      +'  </label>'
      +'  <label class="item item-input">'
      +'    <span class="input-label">License Plate State</span>'
      +'    <div>{{vCopy.licensePlate.state}}</div>'
      +'  </label>'
      +'  <label class="item item-input">'
      +'    <span class="input-label">Make</span>'
      +'    <div>{{vCopy.make}}</div>'
      +'  </label>'
      +'  <label class="item item-input">'
      +'    <span class="input-label">Model</span>'
      +'    <div>{{vCopy.model}}</div>'
      +'  </label>'
      +'  <label class="item item-input">'
      +'    <span class="input-label">Year</span>'
      +'    <div>{{vCopy.year}}</div>'
      +'  </label>'
      +'  <label class="item item-input">'
      +'    <span class="input-label">Color</span>'
      +'    <div>{{vCopy.color.name}}</div>'
      +'  </label>'
      +'  <label class="item item-input">'
      +'    <span class="input-label">Transmission</span>'
      +'    <div>{{vCopy.transmission.name}}</div>'
      +'  </label>'
      +'  <label class="item item-input">'
      +'    <span class="input-label">Vehicle Type</span>'
      +'    <div>{{vCopy.vehicleType.name}}</div>'
      +'  </label>'
      +'</div>'
    );

    $templateCache.put('vehicle-info-vendors.html',
      // V I N #
      '<div class="list list-inset">'
      +'  <div class="item item-divider item-image">VIN <i class="subdued">({{inputLength || 0}} of 17)</i></div>'
      +'  <label class="item item-input" ng-click="openAssistedInput(0, $event)">'
      +'    <i class="icon super-search placeholder-icon assertive"></i>'
      +'    <div class="item-text-wrap item-icon-right">'
      +'      <input type="text" ng-model="vCopy.vin" />'
      +'      <div ng-class="{\'icon item-avatar\':vinPics.staged.file || vinPics.request.filename,'
      +'                    \'icon super-ios7-camera-outline assertive\':!vinPics.staged.file && !vinPics.request.filename}">'
      +'        <img ng-if="vinPics.staged.file" ng-src="{{vinPics.staged.file.localFilePath}}" />'
      +'        <img ng-if="vinPics.request.filename && !vinPics.staged.file" ng-src={{imgFilepath}}{{vinPics.request.filename}} />'
      +'      </div>'
      +'    </div>'
      +'  </label>'
      +'</div>'

      // L I C E N S E   P L A T E
      +'<div class="list list-inset">'
      +'  <span class="item item-divider item-image">License Plate</span>'
      
          // NUM #
      +'  <label class="item item-input" ng-click="openAssistedInput(1, $event)">'
      +'    <i class="input-label">Num #:</i>'
      +'    <div class="item-text-wrap item-icon-right">'
      +'      <input type="text" ng-model="vCopy.licensePlate.number" />'
      +'      <i ng-class="{\'icon item-avatar\':licPlatePics.staged.file || licPlatePics.request.filename,'
      +'                    \'icon super-image stable\':!licPlatePics.staged.file && !licPlatePics.request.filename}">'
      +'        <img ng-if="licPlatePics.staged.file" ng-src="{{licPlatePics.staged.file.localFilePath}}" />'
      +'        <img ng-if="licPlatePics.request.filename && !licPlatePics.staged.file" ng-src={{imgFilepath}}{{licPlatePics.request.filename}} />'
      +'      </i>'
      +'    </div>'
      +'  </label>'

          // STATE
      +'  <label class="item item-input item-select">'
      +'    <i class="input-label">State:</i>'
      +'    <select ng-model="vCopy.licensePlate.state">'
      +'      <option selected="!vCopy.licensePlate.state">* Choose *</option>'
      +'      <option ng-repeat="state in states" value={{state.$id}} ng-selected="state.$id==vCopy.licensePlate.state">{{state.name}}</option>'
      +'    </select>'
      +'  </label>'
      +'</div>'

      // V E H I C L E      
      +'<div class="list list-inset">'
      +'  <span class="item item-divider item-image">Vehicle</span>'
          // MAKE
      +'  <label class="item item-input">'
      +'    <i class="input-label">Make:</i>'
      +'    <input class="list" type="text" ng-model="vCopy.make" ng-value="vCopy.make">'
      +'  </label>'

          // MODEL
      +'  <label class="item item-input">'
      +'    <i class="input-label">Model:</i>'
      +'    <input type="text" ng-model="vCopy.model" ng-value="vCopy.model">'
      +'  </label>'

          // YEAR
      +'  <div class="item item-range">'
      // +'    <i class="input-label">Year: {{vCopy.year}}</i>'
      // +'    <div class="range range-light">'
      // +'      <span class="item-note">{{priorYears[0]}}</span>'
      +'      <input type="range" name="year"'
      +'                          id="fader"'
      +'                          min={{priorYears[0]}}'
      +'                          max={{priorYears[priorYears.length-1]}}'
      +'                          ng-model="vCopy.year"'
      +'                          ng-value="vCopy.year" />'
      +'      <i class="icon item-note">{{vCopy.year}}</i>'
      // +'    </div>'
      +'  </div>'

          // COLOR
      +'  <div class="item">'
      +'    <i class="input-label">Color: {{vCopy.color.hexCode | colorName}}</i>'
      +'    <div class="item-text-wrap">'
      +'      <button ng-repeat="color in colors"'
      +'              ng-class="{\'button-icon icon {{color.mood}} super-ios7-circle-filled\':vCopy.color.hexCode == color.iid,'
      +'                         \'button-icon icon {{color.mood}} super-ios7-circle-outline\':vCopy.color.hexCode != color.iid}"'
      +'              ng-click="setValue(vCopy.color, \'hexCode\', color.iid, $event)">'
      +'      </button>'
      +'    </div>'
      +'  </div>'
      
        // TRANSMISSION
      +'  <div class="item">'
      +'    <i class="input-label"><b>Transmission</b></i>'
      +'    <div>'
      +'      <a ng-repeat="transmission in transmissions"'
      +'         ng-class="{\'button button-balanced\':transmission.iid == vCopy.transmission.id,'
      +'                    \'button button-outline button-balanced\':transmission.iid != vCopy.transmission.id}"'
      +'         ng-click="setValue(vCopy.transmission, \'id\', transmission.iid, $event)">'
      +'          {{transmission.name}}'
      +'      </a>'
      +'    </div>'
      +'  </div>'
        
        // DRIVETRAIN
      +'  <div class="item">'
      +'    <span class="input-label"><b>Drivetrain</b></span>'
      +'    <span class="button-bar">'
      +'      <a ng-repeat="drivetrain in drivetrains"'
      +'              ng-class="{\'button button-small button-balanced\':drivetrain.iid == vCopy.drivetrain.id,'
      +'                         \'button button-small button-outline button-balanced\':drivetrain.iid != vCopy.drivetrain.id}"'
      +'              ng-click="setValue(vCopy.drivetrain, \'id\', drivetrain.iid, $event)">{{drivetrain.acronym}}'
      +'      </a>'
      +'    </span>'
      +'  </div>'

          // VEHICLE TYPE
      +'  <label class="item item-input item-select">'
      +'    <i class="input-label">Vehicle Type:</i>'
      +'    <select ng-model="vCopy.vehicleTypeId">'
      +'      <option ng-repeat="vehicleType in vehicleTypes"'
      +'              value={{vehicleType.id}}'
      +'              ng-selected="vehicleType.iid == vCopy.vehicleType.id">'
      +'                 {{vehicleType.name}}'
      +'      </option>'
      +'    </select>'
      +'  </label>'
      +'</div>'

      // ACTION BUTTONS
      +'<div class="list-inset">'
      +'  <button ng-class="{\'button-block button-small button-light\':!compareObjects(vCopy, request.vehicle),'
      +'                     \'button-block button-small button-calm\':compareObjects(vCopy, request.vehicle)}"'
      +'          ng-click="saveInfo(vCopy)" ng-disabled="compareObjects(vCopy, request.vehicle)"><h3>Save</h3>'
      +'  </button>'
      +'  <button class="button button-block button-small button-assertive" ng-click="clearInfo(vCopy)"><h3>Clear All</h3></button>'
      +'</div>'
    );

    $templateCache.put("police-info-clients.html",
      '<div class="list list-inset">'
      +'  <span class="item item-divider">Police Information</span>'
      +'  <label class="item item-input">'
      +'    <span class="input-label">Police Dept: </span>'
      +'    <div>Santa Cruz County Police</div>'
      +'  </label>'
      +'  <label class="item item-input">'
      +'    <span class="input-label">Case #</span>'
      +'    <div>114-2-2212</div>'
      +'  </label>'
      +'  <label class="item item-input">'
      +'    <span class="input-label">Date Filed</span>'
      +'    <div>Feb 1, 2015 @ 11:45 PM</div>'
      +'  </label>'
      +'</div>'
    );
    
    $templateCache.put('police-info-vendors.html',
      '<div class="list list-inset">'
      +'  <span class="item item-divider item-image">Police Information</span>'
      +'  <label class="item item-input item-select">'
      +'    <i class="input-label">Station:</i>'
      // +'    <select ng-model="">'
      +'    <select>'
      // +'      <option selected="!vCopy.licensePlate.state">* Choose *</option>'
      // +'      <option ng-repeat="state in states" value={{state.$id}} ng-selected="state.$id==vCopy.licensePlate.state">{{state.name}}</option>'
      +'    </select>'
      +'  </label>'

          // CASE NUM #
      +'  <label class="list item item-input">'
      +'    <i class="input-label">Case #:</i>'
      // +'    <input type="text" ng-model="" ng-value="">'
      +'    <input type="text" />'
      +'  </label>'

          // ACTION BUTTONS
      +'  <div class="list list-inset">'
      +'    <button ng-class="{\'button button-outline button-block button-small button-light\':true,'
      +'                       \'button button-outline button-block button-small button-calm\':false}"'
      +'            ng-click="" ng-disabled="">Save'
      +'    </button>'
      // +'    <button class="button button-block button-small button-assertive" ng-click="">Clear All</button>'
      +'    <button class="button button-block button-small button-assertive">Clear All</button>'
      +'  </div>'
      +'</div>'
    );

    // setup Nav stuff
    // var buttons = ["back", "menu"];
    var buttons = ["back"];
    var subViews = ['main','assistedInput','showStatus','chooseStatus','signature'];
    var thisView = Nav.parseViewName(steroids.view.location);

    Nav.initSubViews(subViews);
    Nav.switchSubView("main");
    Nav.setButtons(buttons);

    buttons[0] = Nav.initButtons('back', "back2.png", "left", 1, Nav.setupButton);
    // buttons[1] = Nav.initButtons('menu', "menu4.png", "right", 1, Nav.setupButton);
    // buttons[1].navBtn.onTap = function() {
    //   Nav.enterView("modal", Nav.modalOnTapOptions("menu"));
    // }

    steroids.view.navigationBar.show({
      title: "Request"
    });
  }])
  .controller('DetailsCtrl', ['$firebase','$q','$rootScope','$scope','$filter','DB'
                            ,'Map','$templateCache','LookupSet','Nav','ORGTYPE_CONST'
                            ,'PICTYPE_CONST','Position','Profile','Request','supersonic'
                            ,'User','Util','Watcher', function($firebase,$q,$rootScope
                            ,$scope,$filter,DB,Map,$templateCache,LookupSet,Nav
                            ,ORGTYPE_CONST,PICTYPE_CONST,Position,Profile,Request
                            ,supersonic,User,Util,Watcher) {
    var backBtn = Nav.getButton("back");
    var thisView = Nav.parseViewName(steroids.view.location);
    var qRequest = $q.defer();
    var qConstituentSite = $q.defer(), qEmployerSite = $q.defer(), qEmployerUsers = $q.defer();

    Nav.initPreloadView("modal");

    angular.extend($scope, {
      chosenPicType: {
        id: null
      },
      distance: null,
      employerImgFilepath: "",
      employerSite: null,
      requirements: {
        info: false,
        owner: false,
        police: false
      },
      statusVisible: false,
      request: null,
      subViews: Nav._getSubViews(),
      themeColor: null,
      vCopy: null,
      test: function() {
        console.log("chosenPicType.id: ", $scope.chosenPicType.id);
        console.log("currentGeoPoint: ", $rootScope.currentGeoPoint);
        console.log("myEmployerId: ", $rootScope.myEmployerId);
        console.log("myOrgTypeId: ", $rootScope.myOrgTypeId);
        console.log("request: ", $scope.request);
        console.log("widgets: ", $scope.widgets);
        console.log("vCopy: ", $scope.vCopy);
        console.log("subViews: ", $scope.subViews['main']);
      }
    });

    backBtn.navBtn.onTap = function() {
      Nav.exitView(thisView, supersonic.ui.layers.pop);
      $scope.vCopy = null;
      $scope.request = null;
    }

    function _startGeoWatcher(message) {
      var stopGeoWatcher = supersonic.data.channel("locationData").subscribe( function(message) {
        Position._setGeoPoint($rootScope.currentGeoPoint = message.content.geoPoint);
        $rootScope.$digest();
        
        // $rootScope.qRequest.promise.then(function() {
        qRequest.promise.then(function(request) {
          Position.getDistances(message.content.geoPoint, [request.geoPoint], function(resp, status) {
            $scope.distance = resp.rows[0].elements[0];
            
            // change empty strings to null
            Util.changeToNull(request.pics, "filename");
            // Request._setRequest($scope.request = request);
            $scope.$digest();
          });
        });
      });

      Watcher._addToUnwatchers(stopGeoWatcher);
    }

    function _startMainWatcher() {
      var stopMainWatcher = supersonic.data.channel(thisView +"Data").subscribe( function(message) {
        var requiredPicTypeIds = _.pluck(LookupSet._getLookupSet("picTypes"), "iid");
        var request = Request._getRequest();

        stopMainWatcher();
        Nav.startView("modal");
        Request._setRequiredPicTypeIds(requiredPicTypeIds);
        _startGeoWatcher(message);
        // check for existing request, and whether it's Id matches
        // if ( $scope.request && $scope.request.$id == message.content.reqId )
        //   return;
        // else
        //   qEmployerUsers.promise.then(function() {
        //     _retrieveRequest(message);  
        //   });
        if (request && request.$id == message.content.reqId) {
          // assumption is that all dependencies were previously obtained
          $scope.vCopy = angular.copy(request.vehicle);
          qRequest.resolve($scope.request = request);
        } else {
          _retrieveSites(message);
          _retrieveEmployerUsers();
          _retrieveRequest(message);
        }

        _publishToPrevView(message);

        Watcher.addToWatchers(_startMainWatcher);
      });
    }

    $rootScope.qMyOrgTypeId.promise.then(function() {
      var buttons = Nav.getButtons();

      $scope.themeColor = Profile._getParam("colors")[$rootScope.myOrgTypeId];
      // if ($rootScope.myOrgTypeId == ORGTYPE_CONST.CLIENT) {
      //   var createBtn = Nav.initButtons('create', "add3.png", "right", 0, Nav.setupButton);
      //   Nav.setButton(createBtn);
      //   createBtn.navBtn.onTap = function() {
      //     Nav.enterView("modal", Nav.modalOnTapOptions("create"));
      //   }
      // }

      steroids.view.navigationBar.update({
        styleClass: "super-navbar",
        overrideBackButton: true,
        buttons: {
          left: _.pluck(_.where(buttons, {side: "left"}), "navBtn"),
          right: _.pluck(_.where(buttons, {side: "right"}), "navBtn")
        }
      });
    });

    function _retrieveEmployerUsers() {
      $q.all([qConstituentSite.promise, qEmployerSite.promise]).then(function(dependencies) {
        var userInfo = Profile._getParam("userInfo");
        var constituentSite = dependencies[0];
        var employerSite = dependencies[1];
  
        User.setupEmployerUsers(employerSite, $rootScope.myOrgTypeId).then(function(users) {
          User.setEmployerUsers(users);
          qEmployerUsers.resolve(users);
        });
      });
    }

    function _retrieveSites(message) {
      // retrieve employer site
      var employerOrgTypeName = Profile.getOrgTypeName($rootScope.myOrgTypeId);
      
      // retrieve employer site
      Profile.retrieveSite($rootScope.myEmployerId, employerOrgTypeName)
        .then(function(employerSite) {
          Profile._setSites($rootScope.myOrgTypeId, [employerSite]);
          qEmployerSite.resolve(employerSite);
        });

      // retrieve constituent site
      var constituentOrgTypeId = Profile.toggleOrgTypeId($rootScope.myOrgTypeId);
      var constituentOrgTypeName = Profile.getOrgTypeName(constituentOrgTypeId);

      Profile.retrieveSite(message.content.constituentSiteId, constituentOrgTypeName)
        .then(function(constituentSite) {
          Profile._setSites(constituentOrgTypeId, [constituentSite]);
          qConstituentSite.resolve(constituentSite);
        });
    }

    function _publishToPrevView() {
      var readyParams = {
        sender: Nav.parseViewName(steroids.view.location),
        content: {}
      };

      supersonic.data.channel("detailsReady").publish(readyParams);
    }

    function _retrieveRequest(message) {
      // retrieve request
      Request.retrieveRequest(message.content.reqId).then(function(fbReq) {
        // get distance info for retrieved request        
        $scope.vCopy = angular.copy(fbReq.vehicle);
        var picTypes = LookupSet._getLookupSet("picTypes");
        var stagedPics = _.map(picTypes, function(picType){
          angular.extend(picType, {
            dbRecord: null,
            file: null,
            isSet: false
          });

          return picType;
        });

        Request._setStagedPics(stagedPics);
        Util.changeToNull(fbReq.pics, "filename");
          
        Position.getDistances($rootScope.currentGeoPoint, [fbReq.geoPoint], function(resp, status) {
          $scope.distance = resp.rows[0].elements[0];
          
          // change empty strings to null
          // Util.changeToNull(fbReq.pics, "filename");
          // Request._setRequest($scope.request = fbReq);
          // $scope.$digest();
        });
        
        // $rootScope.qRequest.resolve();
        qEmployerUsers.promise.then(function() {
          Request._setRequest($scope.request = fbReq);
          qRequest.resolve(fbReq);
        });
      });
    }

    _startMainWatcher();
  }])
  .directive('policeInfoWidget', function() {
    return {
      restrict: 'EA',
      scope: {},
      template: "<div id='police_info' ng-include='policeInfoTemplate'></div>",
      controller: ['$filter','$rootScope','$scope','Host','LookupSet','Nav','PICTYPE_CONST'
                  ,'Profile','Request', function($filter,$rootScope,$scope,Host
                  ,LookupSet,Nav,PICTYPE_CONST,Profile,Request) {
        var myOrgTypeName = Profile.getOrgTypeName($rootScope.myOrgTypeId);

        angular.extend($scope, {
          clearInfo: null,
          form: null,
          policeInfoTemplate: "police-info-" +myOrgTypeName +".html",
          clearInfo: function(vCopy) {
          },
          compareObjects: function(a, b) {
            return _.isEqual(a, b);
          },
          saveInfo: function(newVehicleInfo) {
          },
          setValue: function(object, param, value, e) {
            object[param] = value;
            e.preventDefault();
          },
          test: function() {
            $scope.flashMsg = licPlatePic.staged.file;
          }
        });
      }]
    }
  })
  .directive("initStatusUpdate", ['$rootScope','LookupSet','Nav','Profile','Request'
                              ,'Watcher',function($rootScope,LookupSet,Nav,Profile
                              ,Request,Watcher) {
    return {
      restrict: "EA",
      scope: {
        request: '=model',
        chosenStatusId: '=',
        statusVisible: '='
      },
      template: '<button class="button button-block button-assertive" ng-click="beginUpdate()">Update Status</button>',
      link: function(scope, el, attrs) {
        var userInfo;

        var unWatch = scope.$watch("request.statuses", function(statusEntries) {
          if ( angular.isDefined(statusEntries) && statusEntries != null ) {
            var lastStatusEntry = _.last(statusEntries);
            var nextStatuses = Request.extractNextStatuses(lastStatusEntry);
            
            if (!nextStatuses || nextStatuses.length < 1)
              scope.statusVisible = false;
            else
              scope.statusVisible = true;
          }
        });

        if (angular.isDefined(scope.request) ) {
          var req = scope.request;
          userInfo = Profile._getParam("userInfo");
          
          angular.extend(scope, {
            beginUpdate: function() {
              localStorage.setItem('reqId', scope.request.$id); // this should occur when going from summary --> details view
              Nav.enterView("modal", Nav.modalOnTapOptions("setStatus", supersonic.ui.layers.push));
            }
          });
        }

        Watcher._addToUnwatchers(unWatch);
      }
    };
  }])
  .directive('statusEntry', function() {
    return {
      restrict: 'EA',
      template:
          // Current status
        '<span class="col col-70 item-note">'
        +'  <b>{{statusEntry.statusId | statusUIName}}</b><br />'
          // User || Site Info
        +'  <i ng-if="author">{{author.name.first}} {{author.name.last}}</i>'
        +'  <i ng-if="site">{{site.name}}</i><br />'
        +'  {{statusEntry.timestamp | date : "EEE, MMM d y"}} @{{statusEntry.timestamp | date : "shortTime"}}'
        +'</span>'

           // notes && media icons
        +'<span class="col col-10">'
        // +'    <a href="#" class="subdued" ng-show="statusEntry.notes">'
        +'  <a href="#" class="item-note">'
        +'    <i ng-class="{\'icon calm super-document-text\':statusEntry.notes,'
        +'                  \'icon stable super-document-text\':!statusEntry.notes}">'
        +'    </i>'
        +'  </a><br />'
        // +'    <a href="#" class="subdued" ng-show="request.pics.length">'
        +'  <a href="#" class="item-note">'
        +'    <i ng-class="{\'icon calm super-images\':statusPics.length,'
        +'                  \'icon stable super-images\':!statusPics.length}">'
        +'      {{statusPics.length || 0}}'
        +'    </i>'
        +'  </a>'
        +'</span>'
        
        //    // User || site Photo
        +'<span class="col col-20 item-avatar-right">'
        +'  <img ng-src={{picFilepath}}{{author.filename}} ng-if="author" />'
        +'  <img ng-src={{picFilepath}}{{site.filename}} ng-if="site" />'
        +'</span>',
      controller: ['$rootScope','$scope','DB','Host','LookupSet','Profile','Request'
                  ,'User', function($rootScope,$scope,DB,Host,LookupSet,Profile
                  ,Request,User) {
        var myOrgTypeId = $rootScope.myOrgTypeId;
        var shownCustodianRules = LookupSet._getLookupSet('orgTypes', myOrgTypeId).shownCustodian;
        var author = $scope.statusEntry.author;
        var requestPics = Request._getRequest().pics;

        angular.extend($scope, {
          statusPics: _.where(requestPics, {statusId: $scope.statusEntry.statusId})
        });

        if (myOrgTypeId == author.orgTypeId) { //e.g. author is Vendors && current user is Vendor
          _addUserToScope(User.getEmployerUsers(author.id));
          angular.extend($scope, {
            picFilepath: Host.buildFilepath('users', 'avatar', 'download')
          });
        } else { //e.g. author is Vendor, current user is Client
          var constituentOrgTypeId = Profile.toggleOrgTypeId(myOrgTypeId);
          var constituentOrgTypeName = Profile.getOrgTypeName(constituentOrgTypeId);
          var shownCustodianRule = _.find(shownCustodianRules, {targetOrgTypeId: constituentOrgTypeId})
          
          if (shownCustodianRule.type == "users") {
            // current user allowed to see status author -- who is opposite orgType
            // 1. check current bank of constituent users
            var constituentUser = User.getConstituentUser(author.id);
            
            if (constituentUser) {
              _addUserToScope(constituentUser);
            } else { // 2. if user not found, then retrieve user from DB
              User.setupConstituentUser(author.id).then(function(user) {
                User.addConstituentUser(user);
                _addUserToScope(user);
              });
            }

            angular.extend($scope, {
              picFilepath: Host.buildFilepath('users', 'avatar', 'download')
            });
          } else { // current user can only see author's employer site
            var constituentSites = Profile._getSites(constituentOrgTypeId);
            var request = Request._getRequest();
            var constituentSiteId = request[constituentOrgTypeName].id;
            angular.extend($scope, {
              site: _.find(constituentSites, {$id: constituentSiteId}),
              picFilepath: Host.buildFilepath(constituentOrgTypeName, 'avatar', 'download')
            });
          }
        }

        function _addUserToScope(user) {
          angular.extend($scope, {
            author: user
          });
        }
      }]
    }
  })
  .directive("statusHistory", function() {
    return {
      restrict: "EA",
      scope: {
        statusEntries: "=model",
        themeColor: "@",
        title: "@"
      },
      template:
        "<div class='list list-inset'>"
        +"  <div class='item item-divider item-image item-{{themeColor}}'>{{title}}</div>"
        +"  <div status-entry class='item row'"
        +"                    ng-repeat='statusEntry in statusEntries.slice().reverse() | statusesByOrgType:\"read\":\"statusId\"'>"
        +"  </div>"
        +"</div>",
      controller: ['$scope','Host', function($scope,Host) {
        angular.extend($scope, {
          userFilepath: Host.buildFilepath('users', 'avatar', 'download')
        });
      }]
    }
  })
  .directive('vehicleOptions', function() {
    return {
      restrict: 'EA',
      scope: {
        chosenPicType: "=",
        request: "=model",
        themeColor: "@",
        vCopy: "=" /* object copy of Vehicle data meant for 2-way binding between
                  root-level view && vehicleInfoWidget directive */
      },
      template:
        // widget controls
        '<div class="list item-image row">'
            // V E H I C L E   I N F O
        +'  <span class="col col-5"></span>'

        +'  <span ng-click="switchWidget(\'info\')"'
        +'        ng-class="{\'button-icon col col-20 icon super-model-s {{themeColor}}\':widgets[\'info\'],'
        +'                   \'button-icon col col-20 icon super-model-s\':!widgets[\'info\']}">'
        +'        <i>Info</i>'
        +'  </span>'
        +'  <span class="col col-5"></span>'

            // M A P
        +'  <span ng-click="switchWidget(\'map\')"'
        +'        ng-class="{\'button-icon col col-20 icon super-ios7-location {{themeColor}}\':widgets[\'map\'],'
        +'                   \'button-icon col col-20 icon super-ios7-location-outline\':!widgets[\'map\']}"><br />'
        +'        <i>Map</i>'
        +'  </span>'
        +'  <span class="col col-5"></span>'

            // O W N E R
        +'  <span ng-click="switchWidget(\'owner\')"'
        // +'        ng-class="{\'button-icon col col-20 icon super-ios7-contact calm\':widgets[\'owner\'],'
        // +'                   \'button-icon col col-20 icon super-ios7-contact-outline\':!widgets[\'owner\']}"><br />'
        +'        ng-class="{\'button-icon col col-20 icon super-card {{themeColor}}\':widgets[\'owner\'],'
        +'                   \'button-icon col col-20 icon super-card\':!widgets[\'owner\']}"><br />'
        +'        <i>Owner</i>'
        +'  </span>'
        +'  <span class="col col-5"></span>'

            // P O L I C E   I N F O
        +'  <span ng-click="switchWidget(\'police\')"'
        +'        ng-class="{\'button-icon col col-25 icon super-ios7-star {{themeColor}}\':widgets[\'police\'],'
        +'                   \'button-icon col col-25 icon super-ios7-star-outline\':!widgets[\'police\']}"><br />'
        +'        <i>Police</i>'
        +'  </span>'

            // P R I N T
        // +'  <button ng-click="switchWidget(\'print\')"'
        // +'    ng-class="{\'button-icon icon super-ios7-printer energized\':widgets[\'print\'],'
        // +'               \'button-icon icon super-ios7-printer-outline\':!widgets[\'print\']}">'
        // +'  </button>'
        +'</div>'

        // actual widgets
        +'<div vehicle-info-widget chosen-pic-type="chosenPicType"'
        +'                         indicator="requirements.police"'
        +'                         model="request.vehicle"'
        +'                         v-copy="vCopy"'
        +'                         ng-show="widgets[\'info\']">'
        +'</div>'
        +'<div vehicle-map-widget id="map_details"'
        +'                        ng-if="targetSite"'
        +'                        ng-show="widgets[\'map\']"'
        +'                        model="mapParams"'
        +'                        req="request"'
        +'                        target-site="targetSite">'
        +'</div>'
        +'<div pics-mgr pic-class="list card"'
        +'              ng-show="widgets[\'owner\']"'
        +'              indicator="requirements.owner">'
        +'</div>'
        +'<div police-info-widget ng-show="widgets[\'police\']"'
        +'                   indicator="requirements.police">'
        +'</div>',
      controller: ['$q','$filter','$rootScope','$scope','LookupSet','Map','Nav'
                  ,'Profile','ORGTYPE_CONST', function($q,$filter,$rootScope,$scope
                  ,LookupSet,Map,Nav,Profile,ORGTYPE_CONST) {
        var mapInitialized = false;
        var myOrgTypeName = $filter('orgType')($rootScope.myOrgTypeId, "name");
        var _widgetNames = ['info','map','owner','police'];

        Nav.initWidgets(_widgetNames);
        angular.extend($scope, {
          bounds: null,
          mapParams: null,
          setMapParams: null,
          targetSite: null,
          widgets: Nav._getWidgets(),
          test: function() {
            console.log($scope.widgets);
          },
          setParams: null,
          switchWidget: null
        });

        $scope.switchWidget = function(widgetName) {
          if (!mapInitialized)
            initMap();
  
          Nav.switchWidget(widgetName);
          $scope.widgets[widgetName] = true;
        }

        $scope.setMapParams = function() {
          // prep all sites for setting up Map
          var clients = Profile._getSites(ORGTYPE_CONST.CLIENT);
          var qMap = $q.defer();
          
          $scope.request.iconFilepath = "/icons/requests/" +myOrgTypeName +".png";
          
          if (angular.isDefined($scope.request.clients)) {
            $scope.targetSite = _.findWhere( clients, {$id: $scope.request.clients.id} );
            $scope.targetSite.iconFilepath = '/icons/sites/target.png';
          }
          
          var allPoints = [$scope.targetSite.geoPoint, $scope.request.geoPoint];
          var mapCenter = Map.calcCenter(allPoints);
          var containerDims = {
            width: innerWidth,
            height: innerHeight
          };
        
          // prepare all relevant locations for Map processing
          Map.prepMapParams(allPoints, mapCenter, containerDims).then(function(mapParams) {
            $scope.bounds = Map._getBounds();
            $scope.mapParams = mapParams;
            qMap.resolve();
          });

          return qMap;
        }

        function initMap() {
          mapInitialized = true;
          return $scope.setMapParams();
        }
        
        $scope.switchWidget("info");
      }]
    };
  }).directive('vehicleInfoWidget', function() {
    return {
      restrict: 'EA',
      scope: {
        chosenPicType: "=",
        vehicle: "=model",
        vCopy: "="
      },
      template:
        "<div id='vehicle_info' ng-include='vehicleInfoTemplate'></div>",
      controller: ['$filter','$rootScope','$scope','Host','LookupSet','Nav'
                  ,'PICTYPE_CONST','Profile','Request', function($filter,$rootScope
                  ,$scope,Host,LookupSet,Nav,PICTYPE_CONST,Profile,Request) {
        var priorYearCnt = Request._getPriorYearCount();
        var baseYear = moment().format('YYYY') - priorYearCnt;
        var myOrgTypeName = Profile.getOrgTypeName($rootScope.myOrgTypeId);
        var watcherRunning;

        angular.extend($scope, {
          colors: LookupSet._getLookupSet("colors"),
          clearInfo: null,
          drivetrains: LookupSet._getLookupSet("drivetrains"),
          form: null,
          imgFilepath: Host.buildFilepath("requests/vehicle", "base", "download"),
          priorYears: _.map(new Array(priorYearCnt), function(year) {
            return ++baseYear;
          }),
          licPlatePics: {
            request: _.findWhere(Request._getRequest().pics, {picTypeId: PICTYPE_CONST.LICPLATE}),
            staged: Request._getStagedPic(PICTYPE_CONST.LICPLATE)
          },
          vinPics: {
            request: _.findWhere(Request._getRequest().pics, {picTypeId: PICTYPE_CONST.VIN}),
            staged: Request._getStagedPic(PICTYPE_CONST.VIN)
          },
          states: LookupSet._getLookupSet("states"),
          transmissions: LookupSet._getLookupSet("transmissions"),
          vehicleInfoTemplate: "vehicle-info-" +myOrgTypeName +".html",
          vehicleTypes: LookupSet._getLookupSet("vehicleTypes"),
          clearInfo: function(vCopy) {
            angular.forEach(vCopy, function(value, param) {
              $scope.vCopy[param] = null;
            });
          },
          compareObjects: function(a, b) {
            return _.isEqual(a, b);
          },
          openAssistedInput: function(picTypeId, $event) {
            $event.preventDefault();
            $scope.chosenPicType.id = picTypeId.toString();
            Nav.switchSubView("assistedInput");
            supersonic.ui.navigationBar.hide().then(function() {
              supersonic.app.statusBar.show("dark");
            });
          },
          saveInfo: function(newVehicleInfo) {
            $scope.vehicle = null;
            $scope.vehicle = angular.copy(newVehicleInfo);
          },
          setValue: function(object, param, value, e) {
            object[param] = value;
            e.preventDefault();
          },
          test: function() {
            $scope.flashMsg = licPlatePic.staged.file;
          }
        });

        $scope.vCopy.year = moment().year();
      }]
    }
  }).directive('vehicleMapWidget', function() {
    return {
      restrict: 'EA',
      scope: {
        mapParams: '=model',
        req: "=",
        targetSite: "="
      },
      template:
        // '<div id="map_details" animation="am-fade-and-scale"'
        // +'               placement="center"'
        // +'               backdrop="static">'
        '<img ng-if="targetSite && req"'
        +     'ng-src="https://maps.googleapis.com/maps/api/staticmap'
        +     '?center={{mapParams.center.latitude}},{{mapParams.center.longitude}}'
        +     '&zoom={{mapParams.zoomLevel}}'
        // +     '&size={{mapParams.dims.width}}x{{mapParams.dims.height}}&scale=2'
        +     '&size={{mapParams.dims.width}}x{{mapParams.dims.height}}'
        +     '&markers=icon:|{{req.iconFilepath}}|{{req.geoPoint.lat}},{{req.geoPoint.long}}'
        +     '&markers=icon:|{{targetSite.iconFilepath}}|{{targetSite.geoPoint.lat}},{{targetSite.geoPoint.long}}" />',
      controller: ['$scope', function($scope) {
        angular.extend($scope, {
          test: function() {
            console.log($scope);
          }
        });
      }]
    }
  })