angular.module('auth', ['common','TestData'])
  .config(['$httpProvider', function($httpProvider) {
      //Enable cross domain calls
      $httpProvider.defaults.useXDomain = true;
  }])
  .run(function() {
    steroids.view.navigationBar.hide();
    
    // check if user is authenticated
    // Profile._getParam("authRef").$requireAuth().then(function(authInfo) {
    //   Nav.enterView('', {
    //     name: "dismiss",
    //     method: supersonic.ui.initialView.dismiss
    //   });
    // }, function(authErr) {
    //   console.log("user not logged in");
    //   $rootScope.qUserInfo.reject(authErr);
    // });
  })
  .controller('AuthCtrl', ['$firebase','$q','$rootScope','$sanitize','$scope','DB'
                          ,'Host','$http','Nav','LookupSet','Profile','SitesData'
                          ,'supersonic','UserData', function($firebase,$q,$rootScope
                          ,$sanitize,$scope,DB,Host,$http,Nav,LookupSet,Profile
                          ,SitesData,supersonic,UserData) {
    angular.extend($scope, {
      chosenOrgTypeId: "0",
      chosenSiteId: "0",
      credentials: '',
      isLoggedIn: false,
      imgFilepathUser: Host.buildFilepath('users','avatar','download'),
      imgFilepaths: [
        Host.buildFilepath('vendors','avatar','download'),
        Host.buildFilepath('clients','avatar','download')
      ],
      login: function(creds, rememberMe) {
        var qLogin = $q.defer();
        var authRef = Profile._getParam("authRef");

        authRef.$authWithPassword({
          email: $sanitize(creds.email),
          password: $sanitize(creds.password),
          rememberMe: rememberMe
        }).then(function(authResults) {
          Nav.enterView("summary", {
            animation: supersonic.ui.animate("slideFromTop", { duration: 0.3 }),
            method: supersonic.ui.initialView.dismiss,
            destType: "root"
          });
        }, function(error) {
          qLogin.reject(error);
          // need to handle login error!!!
        });
      },
      orgTypeStuff: {
        names: ["VENDORS", "CLIENTS"],
        colors: ["calm", "assertive"]
      },
      policy: "",
      rememberMe: false,
      setOrgTypeId: function(orgTypeId) {
        $scope.chosenOrgTypeId = orgTypeId;
      },
      setSiteId: function(siteId) {
        $scope.chosenSiteId = siteId;
      },
      sites: SitesData.getSites(),
      themeColor: Profile._getParam("colors")[$rootScope.myOrgTypeId],
      users: null
    });

    UserData.getUsers().then(function(users) {
      $scope.users = users;
    });
  }]);