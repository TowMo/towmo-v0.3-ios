angular
  .module("Directives", [])
  .directive('assistedInput', function() {
    return {
      restrict: 'EA',
      scope: {
        chosenPicType: "=",
        inputParam: '=model',
        vCopy: "="
      },
      template:
        '<div class="item item-input-inset">'
        +'  <label class="item-input-wrapper">'
        +'    <input type="text" id="assisted-input" ng-model="inputParam"'
        +'                       ng-value="inputParam"'
        +'                       maxlength={{maxLength}}'
        +'                       ng-change="handleInput()"'
        +'                       placeholder="{{chosenPicType.id | picType:\'name\'}}" />'
        +'  </label>'
        +'  <button ng-if="inputLength == requiredLength && searchable"'
        +'          class="button-icon button-small icon super-ios7-search-strong balanced"'
        +'          ng-click="find(chosenPicType.id)">'
        +'  </button>'
        +'  <button ng-if="inputLength != requiredLength && searchable"'
        +'          class="button-icon button-small icon super-ios7-search stable">'
        +'  </button>'
        +'</div>'
        +'<div pic-processor ng-if="vCopy"'
        +'                   allow-switch-view="true"'
        +'                   pic-type-id={{chosenPicType.id}}'
        +'                   pic-cnt=0'
        +'                   title={{param}}>'
        +'</div>'
        +'<div class="row">'
        +'  <div class="col col-50 col-offset-25">'
        +'    <img class="full-image" ng-show="searching" src="/icons/progress.gif" />'
        +'    <span ng-show="!searching">{{flashMsg}}</span>'
        +'  </div">'
        +'</div">',
      controller: ['$filter','$q','$rootScope','$scope','Edmunds','Host','Request'
                ,'Util', function($filter,$q,$rootScope,$scope,Edmunds,Host,Request
                ,Util) {
        var inputValue = "";
        $scope.chosenPicType.id = $scope.chosenPicType.id;

        angular.extend($scope, {
          flashMsg: "",
          inputLength: 0,
          maxLength: null,
          param: null,
          requiredLength: null,
          searching: false,
          searchable: null,
          find: function(picTypeId) {
            $scope.searching = true;
            Edmunds.getVehicleData($scope.inputParam).then(function(foundVehicleInfo) {
              //Update current vehicleCopy model
              $scope.searching = false;
              $scope.flashMsg = "Vehicle Info obtained";
              $scope.vCopy = Request.convertVehicleData($scope.vCopy, foundVehicleInfo);
            }, function(errResult) {
              $scope.searching = false;
              $scope.flashMsg = errResult;
            });
          },
          handleInput: function() {
            $scope.inputLength = Util.getObjLength($scope.inputParam);
            
            // if ($scope.actionable) {
            //   if (Util.compareIntValues($scope.maxLength, $scope.inputLength))
            //     $scope.minLengthMet = true;
            //   else
            //     $scope.minLengthMet = false;
            // }
          },
          test: function() {
            $scope.chosenPicType.id = Math.abs($scope.chosenPicType.id - 1);
            console.log("param in Assisted Input: ", $scope.inputParam);
            console.log("chosenPicType.id: ", $scope.chosenPicType.id);
          }
        });

        function resetInputLength() {
          $scope.inputLength = 0;
        }

        $scope.maxLength = $filter("picType")($scope.chosenPicType.id, "requiredLength");
        $scope.param = $filter("picType")($scope.chosenPicType.id, "param");
        $scope.requiredLength = $filter("picType")($scope.chosenPicType.id, "requiredLength");
        $scope.searchable = $filter("picType")($scope.chosenPicType.id, "searchable");

        // if (angular.isDefined($scope.pic.staged.file))
        document.getElementById("assisted-input").focus();
        
        $scope.handleInput();
      }]
    }
  })
  .directive("client", ['$filter','Host','Position','Profile','Request', function(
                        $filter,Host,Position,Profile,Request) {
    function _buildStreet(street) {
      return street.number
            +(street.unit == "" ? street.unit : " " +street.unit)
            +" " +street.name
            +" " +street.type;
    }

    return {
      restrict: "EA",
      scope: {
        clients: "=model",
        distance: "="
      },
      template:
        '  <div class="col col-20 item-avatar">'
        +'    <img ng-src="{{clientImgFilepath}}{{client.filename}}" />'
        +'  </div>'
        +'  <div class="col col-60">'
        +'    <b>{{client.name}}</b><br />'
        +'    {{street}}<br />'
        +'    {{client.address.city}}, {{client.address.state}} {{client.address.zip.primary}}'
        +'  </div>'
        +"  <span units-away class='col col-25 item-note'"
        +"                  distance='{{distance.distance.text}}'"
        +"                  eta='{{distance.duration.text}}'>"
        +'  </span>',
      link: function(scope) {
        if (angular.isDefined(scope.clients)) {
          var client = $filter("clientMap")(scope.clients.id);
        
          angular.extend(scope, {
            clientImgFilepath: Host.buildFilepath('clients', 'avatar', 'download'),
            client: client,
            street: _buildStreet(client.address.street)
          });  
        }
      }
    };
  }])
  .directive('datetimeMgr', function() {
    return {
      restrict: 'EA',
      scope: {
        doneFlag: "=indicator",
        firstSeen: "=model"
      },
      template:
        '<div class="list">'
        +'  <span class="item item-divider">When Did You First See Vehicle?</span>'
        
        // widget controls
        +'  <div class="row">'
        +'    <div class="col col-10"></div>'

              // DATE button/icon
        +'    <div class="col col-50" ng-click="switchSubView(\'date\')">'
        +'      <div class="row">'
        +'        <button ng-show="firstNoticed.date != \'-- NONE --\'"'
        +'                ng-class="{\'col col-30 button-icon icon super-ios7-calendar balanced\':subViews[\'date\'],'
        +'                           \'col col-30 button-icon icon super-ios7-calendar-outline balanced\':!subViews[\'date\']}">'
        // +'      <i>{{firstNoticed.date | date : \'mediumDate\'}}</i>'
        +'        </button>'
        +'        <button ng-show="firstNoticed.date == \'-- NONE --\'"'
        +'                ng-class="{\'col col-30 button-icon icon super-ios7-calendar energized\':subViews[\'date\'],'
        +'                           \'col col-30 button-icon icon super-ios7-calendar-outline energized\':!subViews[\'date\']}">'
        // +'      <i>{{firstNoticed.date}}</i>'
        +'        </button>'
        +'        <i class="col col-70 item-note">Date<br />{{firstNoticed.date | date : \'mediumDate\'}}</i>'
        +'      </div>'
        +'    </div>'

        // +'    <div class="col col-5"></div>'

              // TIME button/icon
        +'    <div class="col col-50" ng-click="switchSubView(\'time\')">'
        +'      <div class="row">'
        +'        <button ng-show="firstNoticed.time != \'-- NONE --\'"'
        +'                ng-class="{\'col col-30 button-icon icon super-ios7-clock balanced\':subViews[\'time\'],'
        +'                           \'col col-30 button-icon icon super-ios7-clock-outline balanced\':!subViews[\'time\']}">'
        // +'      <h4>{{firstNoticed.time | date : \'shortTime\'}}</h4>'
        +'        </button>'
        +'        <button ng-show="firstNoticed.time == \'-- NONE --\'"'
        +'                ng-class="{\'col col-30 button-icon icon super-ios7-clock energized\':subViews[\'time\'],'
        +'                           \'col col-30 button-icon icon super-ios7-clock-outline energized\':!subViews[\'time\']}">'
        // +'      <h4>{{firstNoticed.time}}</h4>'
        +'        </button>'
        +'        <i class="col col-70 item-note">Time<br />{{firstNoticed.time | date : \'shortTime\'}}</i>'
        +'      </div>'
        +'    </div>'
        +'  </div>'

        +'  <date-picker ng-show="subViews[\'date\']" model="firstNoticed.date" validator="updateTimestamp(type)"></date-picker>'
        +'  <time-picker ng-show="subViews[\'time\']" model="firstNoticed.time" validator="updateTimestamp(type)"></time-picker>'
        +'</div>',
      controller: ['$scope', function($scope) {
        var _noDateMsg = "--------", _noTimeMsg = "--------";
        angular.extend($scope, {
          firstNoticed: {
            date: _noDateMsg,
            time: _noTimeMsg
          },
          subViews: new Array(2),
          toogleSubView: null,
          updateTimestamp: null
        });

        $scope.subViews["date"] = false;
        $scope.subViews["time"] = true;

        $scope.updateTimestamp = function() {
          $scope.doneFlag = ($scope.firstNoticed.date != _noDateMsg &&
                            $scope.firstNoticed.time != _noTimeMsg)
                            ? true : false;
          
          if ($scope.firstNoticed.date != _noDateMsg)
            $scope.firstSeen.setDate($scope.firstNoticed.date.getDate());
          
          if ($scope.firstNoticed.time != _noTimeMsg) {
            $scope.firstSeen.setHours($scope.firstNoticed.time.getHours());
            $scope.firstSeen.setMinutes($scope.firstNoticed.time.getMinutes());
            $scope.firstSeen.setSeconds($scope.firstNoticed.time.getSeconds());
            $scope.firstSeen.setMilliseconds($scope.firstNoticed.time.getMilliseconds());
          };
        }

        $scope.switchSubView = function(subView) {
          if (!$scope.subViews[subView]) {
            _.forIn($scope.subViews, function(state, subViewName) {
              $scope.subViews[subViewName] = (subView == subViewName) ? true : false
            });
          }
        }

        this.getNoDateMsg = function() {
          return _noDateMsg;
        }

        this.getNoTimeMsg = function() {
          return _noTimeMsg;
        }
      }]
    }
  }).directive('datePicker', function() {
    return {
      restrict: 'EA',
      scope: {
        date1stNoticed: "=model",
        updateTimestamp: "&validator"
      },
      template:
        '<button ng-class="{\'button button-block button-small icon-left button-calm\':previousDays == 0,'
        +'                  \'button button-block button-small icon icon-left button-stable\':previousDays != 0}"'
        +'       ng-click="setDate(0, $event)"><b>Today</b>'
        +'</button>'
        +'<button ng-class="{\'button button-block button-small icon-left button-calm\':previousDays == 1,'
        +'                   \'button button-block button-small icon icon-left button-stable\':previousDays != 1}"'
        +'       ng-click="setDate(1, $event)"><b>Yesterday</b>'
        +'</button>'

        +'<datepicker ng-model="date1stNoticed"'
        +'            ng-change="setDate($event)"'
        +'            max-date="maxDate"'
        +'            show-weeks="false"'
        +'            year-range=1'
        +'            class="well well-lg">'
        +'</datepicker>',
      controller: ['$scope', function($scope) {
        angular.extend($scope, {
          maxDate: moment().toDate(),
          previousDays: -1,
          getType: function(key) {
            return Object.prototype.toString.call($scope[key]);
          },
          setDate: function(previousDays, $event) {
            $event.stopPropagation();

            if (arguments.length > 1) {
              $scope.date1stNoticed = moment().subtract(previousDays, 'day')._d;
              $scope.previousDays = previousDays;
              $scope.updateTimestamp({type:'date'});  
            } else {
              $scope.previousDays = moment(date1stNoticed).fromNow();
            }
          }
        });
      }]
    }
  }).directive('timePicker', function() {
    return {
      require: "^datetimeMgr",
      restrict: 'EA',
      scope: {
        time1stNoticed: "=model",
        updateTimestamp: "&validator"
      },
      template:
        '<div class="list item">'
        +'<div class="range range-calm">'
        +'  <i class="item-note">Hr</i>'
        +'  <input type="range" name="Hr"'
        +'                      min=1'
        +'                      max=12'
        +'                      ng-model="hrStep"'
        +'                      ng-change="setTime(\'setHours\', hrStep)"'
        +'                      ng-value={{hrStep}} />'
        +'  <span class="item-note">{{hrStep}}</span>'
        +'</div>'
        +'<div class="range range-calm">'
        +'  <i class="item-note">Min</i>'
        +'  <input type="range" name="Min"'
        +'                      min=0'
        +'                      max=55'
        +'                      step=5'
        +'                      ng-model="minStep"'
        +'                      ng-change="setTime(\'setMinutes\', minStep)"'
        +'                      ng-value={{minStep}} />'
        +'  <span class="item-note">:{{minStep}}</span>'
        +'</div>'
        +'</div>'

        +'<button ng-class="{\'button button-block button-small icon icon-left super-ios7-sunny button-calm\':chosenMeridiem == \'AM\','
        +'                   \'button button-block button-small icon icon-left super-ios7-sunny-outline button-stable\':chosenMeridiem != \'AM\'}"'
        +'        ng-click="setMeridiem(\'AM\')"><b>AM</b>'
        +'</button>'
        +'<button ng-class="{\'button button-block button-small icon icon-left super-ios7-moon button-calm\':chosenMeridiem == \'PM\','
        +'                   \'button button-block button-small icon icon-left super-ios7-moon-outline button-stable\':chosenMeridiem != \'PM\'}"'
        +'        ng-click="setMeridiem(\'PM\')"><b>PM</b>'
        +'</button>',
      link: function(scope, el, attrs, datetimeCtrl) {
        angular.extend(scope, {
          chosenMeridiem: null,
          hrStep: 1,
          minStep: 5,
          setTime: function(action, stepType) {
            if ( scope.time1stNoticed == datetimeCtrl.getNoTimeMsg() ) {
              scope.time1stNoticed = moment().toDate();
              
              if (!scope.chosenMeridiem)
                scope.chosenMeridiem = scope.time1stNoticed.getHours() < 12
                                      ? "AM" : "PM";
              else
                scope.setMeridiem(scope.chosenMeridiem);
            }

            var unitsToSet = parseInt(stepType) + ( (scope.chosenMeridiem == "AM") ? 0 : 12 );
            scope.time1stNoticed[action](unitsToSet);
            scope.updateTimestamp({type: "time"});
          },
          setMeridiem: function(meridiem) {
            scope.chosenMeridiem = meridiem;
            var currentHours = scope.time1stNoticed.getHours();
            
            if (currentHours > 11 && meridiem == "AM")
              scope.time1stNoticed.setHours(currentHours - 12);
            else if (currentHours < 12 && meridiem == "PM")
              scope.time1stNoticed.setHours(currentHours + 12);
          }
        });
      }
    }
  })
  .directive('map', function() {
    return {
      restrict: 'EA',
      scope: {
        requests: "=model",
        // bounds: "=",
        constituentSites: "=",
        employerSites: "=",
        id: "@"
        // map: "=mapParams"
      },
      controller: ['$element','$filter','$q','$rootScope','$scope','Map'
                  ,'ORGTYPE_CONST', function($element,$filter,$q,$rootScope,$scope
                  ,Map,ORGTYPE_CONST) {
        // prepare all relevant parameters to build Map
        var qMaxZoomLevel;
        var qMap;
        var myOrgTypeName = $filter('orgType')($rootScope.myOrgTypeId, "name");
        var allGeoPoints = ($rootScope.myOrgTypeId == ORGTYPE_CONST.VENDOR)
                            ? Map.extractGeoPoints($scope.employerSites, $scope.constituentSites)
                            : Map.extractGeoPoints($scope.employerSites, $scope.requests);
        var mapEntities = [
          {
            name: 'employerSites',
            entities: $scope.employerSites,
            filepath: "/icons/sites/home.png",
            animation: google.maps.Animation.DROP,
            mapMarkers: new Array($scope.employerSites.length)
          }, {
            name: "constituentSites",
            entities: $scope.constituentSites,
            filepath: '/icons/sites/target.png',
            animation: google.maps.Animation.DROP,
            mapMarkers: new Array($scope.constituentSites.length)
          }, {
            name: "requests",
            entities: $scope.requests,
            filepath: "/icons/requests/" +myOrgTypeName +".png",
            animation: google.maps.Animation.DROP,
            mapMarkers: new Array($scope.requests.length)
          }
        ];

        var containerDims = {
          height: innerHeight,
          width: innerWidth
        };
        var mapCenter = Map.calcCenter(allGeoPoints);
        qMaxZoomLevel = Map.getMaxZoomLevel(mapCenter);

        var bounds = Map.createBounds(allGeoPoints);
        qMaxZoomLevel.then(function(maxZoomLevel) {
          var zoomLevel = Map.calcZoomLevel(bounds,containerDims,maxZoomLevel);
          
          // Build Map
          Map.buildMap(mapCenter, $element[0], zoomLevel).then(function() {
            //2. Cycle through all entities to create Map Markers
            _.each(mapEntities, function(mapEntity) {
              // Map.addIconFilepath(mapEntity);
              Map.createMapMarkers(mapEntity);
            });
          });
        });
      }]
    };
  })
  .directive("menu", function() {
    return {
      restrict: "EA",
      scope: true,
      template:
        // '<div class="item">'
        // +'  <button ng-show="nightMode" class="button button-large button-outline button-light" ng-click="toggleMode(false)">'
        // +'    <i class="icon super-ios7-sunny-outline"></i>'
        // +'  </button>'
        // +'  <button ng-hide="nightMode" class="button button-large button-outline button-energized">'
        // +'    <i class="icon energized super-ios7-sunny"></i>'
        // +'  </button>'

        // +'  <button ng-hide="nightMode" class="button button-large button-outline button-light" ng-click="toggleMode(true)">'
        // +'    <i class="icon super-ios7-moon-outline"></i>'
        // +'  </button>'
        // +'  <button ng-show="nightMode" class="button button-large button-outline button-energized">'
        // +'    <i class="icon energized super-ios7-moon"></i>'
        // +'  </button></h1>'
        // +'  Hello <b>{{user.name.first}} {{user.name.last}}</b>'
        // +'</div>'
        // +'<div class="list">'
        // +'  <a class="item item-icon-left" href="#" ng-click="goToView(\'summary\')">'
        // +'    <i class="icon super-model-s"></i>Requests'
        // +'    <span class="badge badge-balanced">25</span>'
        // +'  </a>'
        // +'  <a class="item item-icon-left" href="#">'
        // +'    <i class="icon super-wand"></i>Admin'
        // +'  </a>'
        // +'  <a class="item item-icon-left" href="#">'
        // +'    <i class="icon super-gear-b"></i>Settings'
        // +'  </a>'
        // +'  <a class="item item-icon-left" href="#">'
        // +'    <i class="icon super-ios7-star"></i>White List'
        // +'    <span class="badge badge-royal">8</span>'
        // +'  </a>'
        // +'  <a class="item item-avatar" href="#" ng-click="goToView(\'profile\')">'
        // +'    <img ng-src="{{clientImgFilepath}}{{user.filename}}" />Profile'
        // +'  </a>'
        // +'  <a class="item item-icon-right" href="#">'
        // +'    <div ng-click="logout()">'
        // +'      <i class="icon super-ios7-locked"></i>Logout'
        // +'    </div>'
        // +'  </a>'
        // +'</div>',
        '<md-sidenav class="md-sidenav-left" md-component-id="left">'
          // +'<md-content class="md-padding">'
          +'<md-content class="md-padding">'
          //   +'<div ng-click="goToView(\'summary\')" class="md-primary">Requests</div>'
          //   +'<div class="md-primary">Assignments</div>'
          //   +'<div class="md-primary">Exempt List</div>'
          //   +'<div ng-click="goToView(\'profile\')" class="md-primary">Profile</div>'
          //   +'<div ng-click="goToView(\'profile\')" class="md-primary">Night Mode</div>'
          //   +'<div ng-click="logout()" class="md-primary">Logout</div>'
          // +'</md-content>'
            +'<md-list>'
              
              // menu items
              +'<md-item ng-repeat="link in links" ng-show="!$last">'
                +'<md-item-content ng-click="goToView(\'profile\')">'
                  +'<div class="md-tile-left">'
                    +'<md-icon md-font-icon={{link.icon}} style="font-size:32px"></md-icon>'
                  +'</div>'
                  +'<div class="md-tile-content">'
                    +'<h3>{{link.name}}</h3>'
                  +'</div>'
                +'</md-item-content>'
                +'<md-divider ng-if="!$last"></md-divider>'
              +'</md-item>'

              // night-mode
              +'<md-item>'
                +'<md-item-content>'
                  +'<div class="md-tile-left">'
                    +'<md-icon md-font-icon="super-ios7-moon-outline" style="font-size:32px"></md-icon>'
                  +'</div>'
                  +'<div class="md-tile-content">'
                    +'<h3>Night Mode</h3>'
                  +'</div>'
                  +'<div class="md-tile-right">'
                    +'<md-switch></md-switch>'
                  +'</div>'
                +'</md-item-content>'
                +'<md-divider></md-divider>'
              +'</md-item>'

              // logout
              +'<md-item>'
                +'<md-item-content ng-click="logout()">'
                  +'<div class="md-tile-left">'
                    +'<md-icon md-font-icon="super-ios7-locked-outline" style="font-size:32px"></md-icon>'
                  +'</div>'
                  +'<div class="md-tile-content">'
                    +'<h3>Logout</h3>'
                  +'</div>'
                +'</md-item-content>'
              +'</md-item>'
            +'</md-list>'
          +'</md-content>'
        +'</md-sidenav>',
      controller: ['$mdSidenav','$rootScope','$scope','Host','Nav','Position'
                  ,'Profile','supersonic', function($mdSidenav,$rootScope,$scope
                  ,Host,Nav,Position,Profile,supersonic) {
        var thisView = Nav.parseViewName(steroids.view.location);  

        angular.extend($scope, {
          clientImgFilepath: Host.buildFilepath('users', 'avatar'),
          // nightMode: true,
          links: [
            {
              name: "Requests",
              icon: "super-model-s"
            }, {
              name: "Profile",
              icon: "super-ios7-person-outline"
            }, {
              name: "Admin",
              icon: "super-wand"
            }, {
              name: "Exempt List",
              icon: "super-ios7-star-outline"
            }
          ],
          user: null,
          goToView: function(viewName) {
            var options, targetView;
            
            if (viewName != "summary") {
              targetView = "modal";
              options = Nav.buildOnTapOptions("modalData", "modal", supersonic.ui.layers.push, {
                targetSubView: viewName,
                geoPoint: Position.getGeoPoint()
              });
            } else {
              targetView = "summary";
              options = Nav.buildOnTapOptions("summaryData", "root", supersonic.ui.layers.popAll, {
                targetSubView: viewName,
                geoPoint: Position.getGeoPoint()
              });
            }

            Nav.enterView(targetView, options);
            $mdSidenav("left").close();
          },
          logout: function() {
            Nav.logout();
          },
          test: function() {
            console.log($scope.user);
          },
          toggleMode: function(status) {
            $scope.nightMode = status;
            document.body.className = $scope.nightMode ? "night" : "day";
          }
        });

        // var closeBtn = Nav.getButton("close");
        // supersonic.ui.views.current.whenVisible(function() {
        //   steroids.view.navigationBar.show({
        //     title: "App Menu"
        //   });

        //   steroids.view.navigationBar.update({
        //     styleClass: "super-navbar",
        //     overrideBackButton: true,
        //     buttons: {
        //       left: [],
        //       right: [closeBtn.navBtn]
        //     }
        //   });
        // });

        $scope.user = Profile._getParam("userInfo");
        $scope.toggleMode($scope.nightMode);
      }]
    }
  })
  .directive('nextStatuses', ['$mdDialog','$rootScope','$templateCache','LookupSet'
                          ,'Nav','ORGTYPE_CONST', 'Profile', 'Request', 'Watcher'
                          ,function($mdDialog,$rootScope,$templateCache,LookupSet
                          ,Nav,ORGTYPE_CONST,Profile,Request,Watcher) {
    return {
      restrict: 'EA',
      scope: {
        statusEntries: '=model',
        chosenStatusId: '=statusId'
      },
      template:
        '<button ng-repeat="status in nextStatuses"'
        +'       ng-class="{\'button icon-left super-ios7-checkmark button-block button-outline button-balanced\':status.iid == chosenStatusId,'
        +'                  \'button icon-left super-ios7-circle-outline button-block button-outline button-light\':status.iid != chosenStatusId}"'
        +'       ng-click="setStatus(status.iid)"><b>{{status.actionName}}</b>'
        +'</button>',

        // '<label class="item item-radio" ng-repeat="status in nextStatuses | orgTypeStatuses:\'write\':\'iid\'">'
        //   +'<input type="radio" name="nextStatuses" ng-model="chosenStatusId" ng-value="status.iid" />'
        //   +'<div class="item-content">{{status.actionName}}</div>'
        //   +'<i class="radio-icon super-ios7-checkmark">{{status.name}}</div>'
        //   // +'<md-content ng-if="!$last"><md-divider></md-divider></md-content>'
        // +'</label>',
      controller: function() {
        // $templateCache.put("promptToClose.html",
        //   '<md-dialog aria-label="Mango (Fruit)">'
        //     +'<md-content class="sticky-container">'
        //       +'<md-subheader class="md-sticky-no-effect">No More statuses to update</md-subheader>'
        //       +'<div class="dialog-content">'
        //         +'<md-button ng-click="closeDialog()" class="md-primary">Okay</md-button>'
        //       +'</div>'
        //     +'</md-content>'
        //   +'</md-dialog>');

        $templateCache.put("dialogFYI.html",
          '<md-dialog aria-label="Mango (Fruit)">'
            +'<md-content class="sticky-container">'
              +'<md-subheader class="md-sticky-no-effect">{{dialogMsg}}</md-subheader>'
              +'<div class="dialog-content">'
                +'<md-button ng-click="closeDialog()" class="md-primary">OK</md-button>'
              +'</div>'
            +'</md-content>'
          +'</md-dialog>');
      },
      link: function(scope, el, attrs) {
        var lastStatusEntry;
        var statusesObtained = false;
        var userInfo = Profile._getParam("userInfo");
        var dialogOptions = {
          controller: "dialogCtrl",
          scope: scope,
          templateUrl: "dialogFYI.html"
        };
        
        angular.extend(scope, {
          dialogMsg: "",
          nextStatuses: null,
          setStatus: function(statusId) {
            scope.chosenStatusId = statusId;
          },
        });

        var unwatch = scope.$watch("statusEntries", function(statusEntries) {
          if ( angular.isDefined(statusEntries) && statusEntries != null ) {
            if (!statusesObtained)
              statusesObtained = true;
            else {
              var lastStatusEntry = _.last(statusEntries);
              
              if (lastStatusEntry.author.id != userInfo.$id) {
                scope.dialogMsg = "Looks like someone else updated the request"
                $mdDialog.show(dialogOptions).then(function() {
                  // var thisView = Nav.parseViewName(steroids.view.location);
                  // Nav.exitView(thisView, supersonic.ui.modal.hide());
                  Nav.leaveModal(supersonic.ui.layers.pop);
                });
              }

              unwatch();
              return;
            }

            _resetNextStatuses(statusEntries);
          }
        });

        function _resetNextStatuses(statusEntries) {
          var lastStatusEntry = _.last(statusEntries);
          scope.nextStatuses = Request.extractNextStatuses(lastStatusEntry);

          if (!scope.nextStatuses) {
            $scope.dialogMsg = "No More statuses to update"
            $mdDialog.show(dialogOptions).then(function() {
              // var thisView = Nav.parseViewName(steroids.view.location);
              // Nav.exitView(thisView, supersonic.ui.modal.hide());
              Nav.leaveModal(supersonic.ui.layers.pop);
            });
            
            return undefined;
          } else {
            scope.chosenStatusId = _.first(scope.nextStatuses).iid;
          }
        }
      }
    }
  }])
  .directive('picsMgr', function() {
    return {
      restrict: 'EA',
      scope: {
        picClass: '@',
        doneFlag: '=indicator'
      },
      template: 
        // '<div class="list">'
        '<span class="item item-divider item-image">What Does Vehicle Look Like?</span>'
        // +'</div>'
        // +'<div>{{flashMsg}}</div>'
        +'<div pic-processor class={{picClass}}'
        +'                   allow-switch-view="false"'
        +'                   checker="checkAllDone()"'
        +'                   ng-repeat="stagedPic in stagedPics"'
        +'                   pic-index="$index"'
        +'                   pic-type-id={{stagedPic.iid}}'
        +'                   title={{stagedPic.name}}>'
        +'</div>',
      controller: ['$scope','Host','Nav','LookupSet','Profile','Request', function(
                  $scope,Host,Nav,LookupSet,Profile,Request) {
        // determine relevant picture types
        var picTypes = LookupSet._getLookupSet("picTypes");
        var stagedPics = _.filter(picTypes, function(pic) {
          return _.contains(Request._getRequiredPicTypeIds(), pic.iid);
        });
          
        angular.extend($scope, {
          flashMsg: "none",
          stagedPics: _.map(stagedPics, function(stagedPic) {
            angular.extend(stagedPic, {
              dbRecord: null,
              file: null,
              isSet: false
            });

            return stagedPic;
          }),
            
          checkAllDone: function() {
            $scope.doneFlag = _.every($scope.stagedPics, "isSet");
          },
          test: function(index) {
            $scope.flashMsg = $scope.stagedPics[index].dbRecord;
          }
        });

        Request._setStagedPics($scope.stagedPics);
      }]
    }
  })
  .directive('picProcessor', function() {
    return {
      restrict: 'EA',
      scope: {
        checkAllDone: '&checker',
        allowSwitchView: "@",       
        picIndex: "=",
        picTypeId: "@",
        title: '@'
      },
      template:
        '<div class="item-image">'
        +'  <img ng-if="pic.staged.file"'
        +'       ng-src="{{pic.staged.file.localFilePath}}" />'
        +'  <img ng-if="!pic.staged.file && pic.request.filename"'
        +'       ng-src="{{imgFilepath}}{{pic.request.filename}}" />'
        +'  <img ng-show="!pic.staged.file && !pic.request.filename"'
        +'       src="/icons/image.png" />'
        +'</div>'
        
        +'<div class="item item-inset">'
        +'  <span class="list calm">'
        +'    <b>{{title}}</b>'
        +'  </span>'
        +'  <span class="item-note">'
        +'    <button class="button icon button-icon button-small super-checkmark balanced"'
        +'       ng-show={{allowSwitchView}}'
        +'       ng-click="switchSubView()">'
        +'    </button>'

        +'    <button class="button icon button-icon button-small super-ios7-camera energized"'
        +'       ng-click="useCamera()">'
        +'    </button>'

        +'    <button ng-show="pic.staged.file"'
        +'       ng-click="removePic()"'
        +'       class="button icon button-icon button-small super-ios7-trash assertive">'
        +'    </button>'
        +'    <button ng-show="!pic.staged.file"'
        +'       class="button icon button-icon button-small super-ios7-trash stable">'
        +'    </button>'
        +'  </span>'
        +'</div>',
      controller: ['$http','$scope','Host','Nav','Position','Profile','Request'
                  ,'supersonic', function($http,$scope,Host,Nav,Position,Profile
                  ,Request,supersonic) {
        var requestPics = Request._getRequest() ? Request._getRequest().pics : null;
        $scope.picTypeId = $scope.picTypeId;

        angular.extend($scope, {
          capturePic: null,
          imgFilepath: Host.buildFilepath('requests/vehicle','base','download'),
          flashMsg: "",
          pic: {
            request: null,
            staged: Request._getStagedPic($scope.picTypeId)
          },
          removePic: null,
          switchSubView: function() {
            supersonic.ui.navigationBar.show();
            Nav.switchSubView("main");
          }
        });

        $scope.pic.request = requestPics
                          ? _.findWhere(requestPics, {picTypeId: $scope.picTypeId})
                          : {filename: null};
        
        $scope.useCamera = function() {
          var user = Profile._getParam("userInfo");
          var imgOptions = {
            destinationType: "fileURI",
            encodingType: "jpg",
            quality: 75,
            saveToPhotoAlbum: false,
            targetWidth: 320,
            targetHeight: 240
          };

          supersonic.media.camera.takePicture(imgOptions).then( function(imageURI){
            window.resolveLocalFileSystemURI(imageURI, function(file) {
              var targetDirURI = "file://" +steroids.app.absoluteUserFilesPath;
              var beg = imageURI.lastIndexOf('/') +1
                ,end = imageURI.lastIndexOf('.')
                ,fileExt = imageURI.substr(end +1);

              var fileName = imageURI.slice(beg,end) +"-" +$scope.picIndex +"." +fileExt;
              var fullFilePath = targetDirURI +"/" +fileName;


              window.resolveLocalFileSystemURI(targetDirURI, function(directory) {
                file.moveTo(directory, fileName, function(movedFile) {
                  var localFilePath = "/" +movedFile.name;

                  // FileService.fileCleanUp();
                  $scope.pic.staged.dbRecord = {
                    iid: $scope.picTypeId,
                    authorId: user.$id,
                    filename: "",
                    geoPoint: Position.getGeoPoint(),
                    picTypeId: $scope.picTypeId,
                    statusId: "",
                    timestamp: moment( moment().toDate() ).format()
                  };

                  $scope.pic.staged.file = {
                    fileType: fileExt,
                    localFilePath: localFilePath,
                    sourceFilename: fullFilePath
                  };

                  $scope.pic.staged.isSet = true;

                  if (angular.isDefined($scope.$parent.stagedPic && $scope.checkAllDone)) {
                    $scope.$parent.stagedPic.isSet = true;
                    $scope.checkAllDone();
                  }

                  $scope.$apply();
                }, function(error) {
                   $scope.flashMessage = error;
                });
              }, function(error) {
                $scope.flashMessage = error;
              });
            }, function(errMessage) {
              $scope.flashMessage = errMessage;
            });
          }, function(err) {
            console.log(err);
          }, imgOptions);
        }

        $scope.removePic = function() {
          // only removes staged pic for now
          $scope.pic.staged.dbRecord = null;
          $scope.pic.staged.file = null;
          $scope.pic.staged.isSet = false;
          if (angular.isDefined($scope.$parent.stagedPic))
            $scope.$parent.stagedPic.isSet = false;

          $scope.checkAllDone();
        }
      }]
    }
  })
  .directive('slidingPics', ['Host','PICTYPE_CONST', function(Host,PICTYPE_CONST) {
    return {
      restrict: 'EA',
      scope: {
        pics: '=model'
      },
      template: '<img ng-src={{imgFilepath}}{{filename}} />',
        // '<div class="container slider">'
        // +'  <img ng-repeat="pic in pics" class="slide slide-animation"'
        // +'      ng-swipe-right="nextPic()" ng-swipe-left="prevPic()"'
        // +'      ng-hide="!isCurrentPicIndex($index)" ng-src="{{imgFilepath}}{{pic.filename}}" />'
        // +'  <a class="arrow prev" href="#" ng-click="nextPic()"></a>'
        // +'  <a class="arrow next" href="#" ng-click="prevPic()"></a>'
        // +'  <nav class="nav">'
        // +'    <div class="wrapper">'
        // +'      <ul class="dots">'
        // +'        <li class="dot" ng-repeat="pic in pics">'
        // +'          <a href="#" ng-class="{\'active\':isCurrentPicIndex($index)}"'
        // +'                      ng-click="setCurrentPicIndex($index);">{{pic.iid}}</a>'
        // +'        </li>'
        // +'      </ul>'
        // +'    </div>'
        // +'  </nav>'
        // +'</div>',

        // '<slick dots="true" infinite="false" speed="300" draggable="true" touch-move="true">'
        // '<slick dots=true inifinite=false init-onload=true data="pics">'
        // +'  <div class="item item-image" ng-repeat="pic in pics">'
        // +'    <img ng-src={{imgFilepath}}{{pic.filename}} />'
        // +'  </div>'
        // +'</slick>',

        // '<ul rn-carousel rn-carousel-index="picIndex" class="carousel" ng-if="pics">'
        // +'  <li ng-repeat="pic in pics.slice().reverse() track by pic.iid" ng-class="\'id-\' +pic.iid">'
        // +'    <img ng-src={{imgFilepath}}{{pic.filename}} class="vehicle-pic-med" />'
        // +'  </li>'
        // +'</ul>'
        // +'<div rn-carousel-indicators ng-if="pics.length > 1" slides="pics" rn-carousel-index="picIndex"></div>'
        // +'<div class="icon super-camera" style="font-size:200px" ng-hide="pics"></div>',
      controller: ['$scope', function($scope) {
        var DEFAULT_VEHICLE_TYPE = 'cart';
        
        angular.extend($scope, {
          filename: _.findWhere($scope.pics, {picTypeId: PICTYPE_CONST.VEHICLE}).filename,
          imgFilepath: Host.buildFilepath('requests/vehicle', 'base', 'download')
        });

        /*********************** logic priority
        1) Do any of the eventEntries have a media item w/ mediaType = 1,
           mediaContextId == 3?
        2) Does vehicleTypeId exist for vehicle?
        3) Default to CSS icon of 'vehicle'
        ***************************************/

        // $scope.vehicleType = DEFAULT_VEHICLE_TYPE;
      }]
    };
  }])
  .directive("status", function() {
    return {
      restrict: "EA",
      template:
        '<span class="list assertive">'
        +'    <b>{{status.statusId | statusUIName}}</b>'
        +'</span>'
        +'<span class="item-note">({{status.timestamp | timeAgo}})</span>'
    };
  })
  .directive("user", function() {
    return {
      restrict: "EA",
      scope: {
        user: "=model"
      },
      template:
        '<i class="item-avatar-left">'
        +'  <img ng-src={{imgFilepathUser}}{{user.filename}} />'
        +'</i>'
        +'<span>'
        +'  <b>{{user.name.first}} {{user.name.last}}</b>'
        +'</span>'
        +'<i class="item-avatar-right">'
        +'  <img ng-src={{imgFilepathEmployer}}{{employerSite.filename}} />'
        +'</i>',
      controller: ['$rootScope','$scope','Host','Profile', function($rootScope
                  ,$scope, Host, Profile) {
        $rootScope.qSites.promise.then(function() {
          var employerOrgTypeName = Profile.getOrgTypeName($rootScope.myOrgTypeId);
          var employerSites = Profile._getSites($rootScope.myOrgTypeId);

          angular.extend($scope, {
            employerSite: _.findWhere(employerSites, {$id: $rootScope.myEmployerId}),
            imgFilepathUser: Host.buildFilepath('users', 'avatar'),
            imgFilepathEmployer: Host.buildFilepath(employerOrgTypeName, 'avatar')
          });
        });
      }]
    };
  })
  .directive('unitsAway', function() {
    return {
      restrict: 'EA',
      scope: {
        distance: "@",
        eta: "@"
      },
      template:
        '<div><i class="icon super-ios7-film-outline"></i> {{distance}}</div>'
        +'<div><i class="icon super-ios7-clock-outline"></i> {{eta}}</div>'
    };
  })
