angular.module("Controllers", [])
  .controller('btmSheetCtrl', ["$scope","$mdBottomSheet","Nav", function($scope
                              ,$mdBottomSheet,Nav) {
    angular.extend($scope, {
      giveAnswer: function(answer) {
        if (answer == true)
          $mdBottomSheet.hide();
        else
          $mdBottomSheet.cancel();
      },
      openModal: function() {
        $mdBottomSheet.hide();
        // Nav.exitView("modal", Nav.modalOnTapOptions("create"));  
        Nav.leaveModal(supersonic.ui.layers.pop)();
      }
    })
  }])
  .controller('dialogCtrl', ['$mdDialog','$scope', function($mdDialog,$scope) {
    angular.extend($scope, {
      closeDialog: function() {
        $mdDialog.hide();
      }
    })
  }])