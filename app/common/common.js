angular.module('common', ['Controllers','Directives','Filters','firebase','ngCordova'
                        ,'ngMaterial', 'supersonic', 'Services', 'TestData'])
  .run(['$firebaseArray','$firebaseObject','$q','$rootScope','$templateCache','DB'
        ,'Host','LookupSet','Nav','Position','Profile','supersonic','Watcher'
        , function($firebaseArray,$firebaseObject,$q,$rootScope,$templateCache,DB
        ,Host,LookupSet,Nav,Position,Profile,supersonic,Watcher) {
    var thisView = Nav.parseViewName(steroids.view.location);
    var qUid = $q.defer();
    var _userInfo;

    //initilize stuff for ALL views
    Host.setHostSource(1);

    angular.extend($rootScope, {
      currentGeoPoint: {
        lat: 0.0,
        long: 0.0,
        latitude: 0.0,
        longitude: 0.0,
        timestamp: ""
      },
      myEmployerId: null,
      myOrgTypeId: null,
      qLookupSets: $q.defer(),
      qMyEmployerId: $q.defer(),
      qMyOrgTypeId: $q.defer(),
      qUserInfo: $q.defer(),
      setOrgTypeId: null
    });

    // if current view is Auth, defer to Auth's .run()
    if (thisView == "auth")
      return;
    else if (thisView != "summary") {
      var coreDataUnwatch = supersonic.data.channel("coreData").subscribe( function(message) {
        var lookupSets = message.content.lookupSets;
        
        if (!LookupSet._getAllLookupSets()) {
          LookupSet._setLookupQty(_.size(lookupSets));

          _.forIn(lookupSets, function(set, index) {
            LookupSet._setLookupSet(index, set);
          });

          $rootScope.qLookupSets.resolve();
        }

        Position._setGeoPoint($rootScope.currentGeoPoint = message.content.geoPoint);
        $rootScope.qLookupSets.resolve(lookupSets);
        coreDataUnwatch();
      });
    }

    $rootScope.setOrgTypeId = function(orgTypeIds) {

      if (orgTypeIds.length == 1) {
        Profile._setParam("myOrgTypeId", _userInfo.state.myOrgTypeId = $rootScope.myOrgTypeId = orgTypeIds[0]);
        _userInfo.$save();
        return orgTypeIds[0];
      } else {
        /*********************************************************************
        *                                                                    *
        *                                                                    *
        *                 Add section to handle multiple partyIds            *
        *                                                                    *
        *                                                                    *
        *********************************************************************/
        
        // $scope.orgTypeIds = orgTypeIds;
        // $scope.switchView('postLogin');
      }
    }

    /********************** VALIDATE USER AUTHENICATED ************************
    ***************************************************************************
    **************************************************************************/
    var authInfo = Profile._getParam("authRef").$getAuth();
    
    if (authInfo != null) {
      // obtain Firebase User info
      var uid = Profile.substrUid(authInfo.uid);
      $firebaseObject(DB.getRef().child('users').child(uid)).$loaded().then(function(userInfo) {
        Profile._setParam("userInfo", _userInfo = userInfo);
        var userState = userInfo.state;

        // PROPERLY SET employerId && orgTypeId
        _.each(["myEmployerId", "myOrgTypeId"], function(param) {
          if (!userState || userState[param] == "") {
            $rootScope[param] = null; 
          } else {
            Profile._setParam(param, $rootScope[param] = userState[param]);
            param = "q" +param.slice(0,1).toUpperCase() + param.slice(1,param.length);
            $rootScope[param].resolve();
          }
        })

        $rootScope.qUserInfo.resolve(userInfo);
      }, function(err) {
        $rootScope.qUserInfo.reject(err);
      });
    } else {
      Nav.enterView("auth", {
        name: "replace",
        method: supersonic.ui.layers.replace
      });
    }

    /***************************** RESOLVE LOOKUPSETS *************************
    ***************************************************************************
    **************************************************************************/
    var lookupSets = LookupSet._getAllLookupSets();
    if (lookupSets != null || lookupSets != undefined)
      $rootScope.qLookupSets.resolve();

    $templateCache.put("menu.html", '<md-bottom-sheet><div menu></div></md-bottom-sheet>');
  }])
  .constant('EVENT_CONST', {
    'EVENT_RELEASE': "9",
    'EVENT_TYPE_WORKFLOW': "1",
    'EVENT_TYPE_CONFIRMATION': "2",
    'RIGHT_TYPE_WRITE': "1",
    'RIGHT_TYPE_VIEW': "2"
  })
  .constant('ORGTYPE_CONST', {
    'VENDOR': "0",
    'CLIENT': "1"
  })
  .constant("PICTYPE_CONST", {
    'VIN': "0",
    'LICPLATE': "1",
    'VEHICLE': "2",
    'DL': "3",
    'REGISTRATION': "4"
  });

angular.module('Filters', [])
  .filter("chosenUser", function() {
    return function(users, siteId) {
    //   var usersBySiteType = _.filter(users, function(user) {
    //     var qualifiedUser = _.find(user.accessRights, {"siteId": siteId});
    //     return qualifiedUser;
    //   });

    //   return usersBySiteType;

      return _.where(users, {"siteId": siteId});
    };
  })
  .filter("usersByOrgType", function() {
    return function(users, orgTypeId) {
      var usersByOrgType = _.filter(users, function(user) {
        var qualifiedUser = _.find(user.accessRights, {"orgTypeId": orgTypeId});
        return qualifiedUser;
      });

      return usersByOrgType;
    };
  })
  .filter("clientMap", ['Profile','ORGTYPE_CONST', function(Profile,ORGTYPE_CONST) {
    return function(clientId) {
      var clientSites = Profile._getSites(ORGTYPE_CONST.CLIENT);
      return _.find(clientSites, {'$id': clientId});
    };
  }])
  .filter("colorName", ['LookupSet', function(LookupSet) {
    return function(hexCode) {
      return LookupSet._getLookupSet("colors", hexCode).name;
    };
  }])
  .filter('orgType', ['LookupSet', function(LookupSet) {
    return function(orgTypeId, param) {
      return LookupSet._getLookupSet('orgTypes', orgTypeId)[param];
    };
  }])
  .filter('orgTypeStatuses', ['LookupSet','Profile',function(LookupSet,Profile) {
    return function(statuses, action, key) {
      if (angular.isDefined(statuses) || statuses != null) {
        var orgTypeId = Profile._getParam("myOrgTypeId");
      
        return _.filter(statuses, function(status) {
          var statusLookup = LookupSet._getLookupSet("statuses", status[key]);
          return (statusLookup.userRights[orgTypeId][action] == true);
        });  
      }
    };
  }])
  .filter('pic', function() {
    return function(pics, picTypeId, param) {
      if ( angular.isDefined(pics) ) {
        var pic = _.findWhere(pics, {"picTypeId": picTypeId});  
        if (angular.isDefined(pic))
          return angular.isDefined(param) ? pic[param] : pic;
      }
      
      return;
    };
  })
  .filter('picType', ['LookupSet', function(LookupSet) {
    return function(picTypeId, param) {
      var picType = LookupSet._getLookupSet('picTypes', picTypeId);
      return picType ? picType[param] : undefined;
    };
  }])
  .filter('statusByOrgType', ['LookupSet','Profile', function(LookupSet, Profile) {
    return function(statuses, action, key) {
      var myOrgTypeId = Profile._getParam("myOrgTypeId");
      
      return _.filter(statuses, function(status) {
        var statusLookup = LookupSet._getLookupSet("statuses", status[key]);
        return (statusLookup.userRights[myOrgTypeId][action] == true);
      });
    };
  }])
  .filter('statusUIName', ['LookupSet', function(LookupSet) {
    return function(statusId) {
       return LookupSet._getLookupSet("statuses", statusId).uiName;
    };
  }])
  .filter('statusesByOrgType', ['LookupSet', 'Profile', function(LookupSet, Profile) {
    return function(statuses, action, key) {
      var orgTypeId = Profile._getParam("myOrgTypeId");
      
      return _.filter(statuses, function(status) {
        var statusLookup = LookupSet._getLookupSet("statuses", status[key]);
        return (statusLookup.userRights[orgTypeId][action] == true);
      });
    };
  }])
  .filter('timeAgo', function() {
    return function(timestamp) {
      return moment(timestamp).fromNow();
    };
  });

angular.module('Services', ['ngSanitize'])
  // .factory("camera")
  .factory('Edmunds', ['$q','$http','$timeout','VehicleData',function($q,$http
                      ,$timeout,VehicleData) {
    var apiKey = 'dq3n4f4xrm6yky44yu23vn6p';
    
    return {
      getVehicleData: function(vin) {
        var qVehicleData = $q.defer();
        
        // $http.get("https://api.edmunds.com/api/vehicle/v2/vins/" +vin
        //   +"?fmt=json&api_key=" +apiKey)
        //   .success(function(foundVehicle) {
        //     if (foundVehicle.hasOwnProperty('warning'))
        //       foundVehicle.vin = vin;

        //       doneVehicleData.resolve(foundVehicle);
        //   })
        //   .error(function(errResults) {
        //     doneVehicleData.reject(errResults);
        //   });

        // return doneVehicleData.promise;
        var vehicle = VehicleData.getVehicle(vin);

        $timeout(function(){
          if ( angular.isDefined(vehicle) )
            qVehicleData.resolve(vehicle);
          else qVehicleData.reject("no Results found");
        }, 1000);

        return qVehicleData.promise;
      }
    }
  }])
  .factory('DB', ["$q","$firebaseArray","$firebaseObject",function($q,$firebaseArray
                ,$firebaseObject) {
    var _dbRef = new Firebase("https://towmo-dev5.firebaseio.com");
    var _childTemplates = ["client","requuest", "police", "user","vehicle", "vendor"];
    _childTemplates["request"] = ["status"];
    _childTemplates["request"]["status"] = {
      timestamp: "",
      geoPoint: {
        lat: 0.0,
        long: 0.0,
        latitude: 0.0,
        longitude: 0.0
      },
      statusId: "",
      author: {
        id: "",
        orgTypeId: ""
      },
      notes: "",
      signFilename: ""
    };

    return {
      getRef: function() {
        return _dbRef;
      },
      getChildTemplate: function(entity, child) {
        return _childTemplates[entity][child];
      },
      getDbRecord: function(path) {
        var qDbRecord = $q.defer();
        var dbRef = _dbRef.child(path);

        dbRef.once("value", function(record) {
          var dbRecord = Array.isArray(record.val())
                      ? $firebaseArray(record.ref())
                      : $firebaseObject(record.ref());

          dbRecord.$loaded().then(function(ngRecord) {
            qDbRecord.resolve(ngRecord);
          });
        }, function(error) {
          console.log(error);
        });

        return qDbRecord.promise;
      },
      destroy: function() {
        _dbRef = null;
      }
    };
  }])
  .factory('Host', function() {
    var _protocols = ["http","https"]
      ,_bucket = "towmo"
      // ,_hostnames = ["://127.0.0.1","://towimg.martiangold.com"]
      ,_hostnames = ["://127.0.0.1","://s3-us-west-1.amazonaws.com/"]
      ,_ports = [3000, ""];

    var _hostSource;

    return {
      setHostSource: function(hostSourceSelection) {
        _hostSource = hostSourceSelection;
      },

      getHostURL: function(source) {
        var baseURL = _protocols[_hostSource || source] +_hostnames[_hostSource || source]
        var port = _ports[_hostSource || source];
        var fullPath = baseURL +( (typeof port == "number") ? ":" +port : "" );
        return fullPath;
      },

      buildFilepath: function(model, size, direction) {
        var size = size || "";
        // var filepath = this.getHostURL() +_bucket +"" +model +size +"/" +direction +"/";
        var filepath = this.getHostURL() +_bucket +"/" +model +"/" +size +"/";
        return filepath;
      },

      uploadFile: function(fileURL, destination) {

      }
    };
  })
  .factory('LookupSet', ['DB','$q', function(DB,$q) {
    var _lookupSets;

    return {
      buildLookupSets: function() {
        var lookupSetKeys = _.keys(_lookupSets);
        var lookupSetsArrayNew = new Array(lookupSetKeys.length);

        _.forIn(lookupSetKeys, function(setName, index) {
          lookupSetsArrayNew[index] = _lookupSets[setName];
        });

        return _.object(lookupSetKeys, lookupSetsArrayNew);
      },
      _getLookupSet: function(setName, rowId) {
        if (arguments.length == 2) {
          return _.findWhere( _lookupSets[setName], {'iid': rowId} );
        } else
          return _lookupSets[setName];
      },

      _getAllLookupSets: function() {
        return _lookupSets;
      },

      _setLookupQty: function(qty) {
        if (typeof _lookupSets == "Array")
          return
        _lookupSets = new Array(qty);
      },

      _setLookupSet: function(setName, set) {
        _lookupSets[setName] = set;
      },

      _setAllLookupSets: function(lookupSets) {
        _lookupSets = lookupSets;
      },

      getRecordByParam: function(set, param, value) {
        var value = value.toLowerCase();
        return _.findWhere(_lookupSets[set], function(record) {
           return (record[param].toLowerCase() === value);
        });
      },

      reloadAllSets: function() { 
        return DB.getRef().child('lookups');
      },

      resolveLookupSets: function(setsToLookup) {
        var cnt = 0;
        var qSets = _.map(setsToLookup, function(set) {
           return $q.defer();
        });

        var self = this;

        _.each(setsToLookup, function(set) {
          if ( !lookupSets[set] ) {
            self.reloadAllSets(set, "/", "").then(function(foundSet) {
              _lookupSets[set] = foundSet.data;
              setsDefer[cnt++].resolve(foundSet.data);
            });
          } else {
            setsDefer[cnt++].resolve();
          }
        });

        return Promise.all(_.map(qSets, function(set) {
           return set.promise;
        }));
      },
      destroy: function() {
        _.each(_lookupSets, function(lookupSet) {
          _.each(lookupSet, function(lookup) {
            lookup = null;
          });
        });
      }
    };
  }])
  .factory('Map', ['$filter','$q','$rootScope','Profile','Request',function($filter
                  ,$q,$rootScope,Profile,Request) {
    var _bounds;
    var _widthToHeight = 2;
    var _maxZoomLevel = 17; // allow to configure via Provider
    var _map;
    
    // var markerIcons = ['sites', 'requests'];
    // markerIcons['requests'] = null;

    //   markerIcons['sites'] = {
    //     path: maps.SymbolPath.CIRCLE,
    //     scale: 7,
    //     fillColor: '#FFFFFF'
    //   };

    //   var markerAnimations = ['sites', 'requests'];
    //   markerAnimations['sites'] = null;
    //   markerAnimations['requests'] = maps.Animation.DROP;

    function calcDims(containerDims) {
      var width = containerDims.width;
      var height = width / _widthToHeight;
      
      return {
        height: height,
        width: width
      };
    }
  
    return {
      addIconFilepath: function(iconEntity) {
        _.each(iconEntity.entities, function(index) {
          index.iconFilepath = iconEntity.filepath;
        });
      },

      buildMap: function(center, mapEl, zoomLevel) {
        //DECLARE MAP BEHAVIOR
        var qMap = $q.defer();
        var mapOptions = {
          center: new google.maps.LatLng(center.lat, center.long),
          zoom: zoomLevel,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        //INSTATIATE MAP OBJECT
        _map = new google.maps.Map(mapEl, mapOptions);
        google.maps.event.addListenerOnce(_map, "tilesloaded", function(){
          qMap.resolve();
        });

        return qMap.promise;
      },

      buildMarkerOptions: function() {
        return {animation: google.maps.Animation.DROP};
      },

      calcCenter: function(geoPoints) {
        // find center based on bounds (which is based on markers)
        var total = geoPoints.length;
        var X = 0, Y = 0, Z = 0;

        _.each(geoPoints, function(geoPoint) {
          var lat = geoPoint.lat * Math.PI / 180;
          var long = geoPoint.long * Math.PI / 180;

          var x = Math.cos(lat) * Math.cos(long);
          var y = Math.cos(lat) * Math.sin(long);
          var z = Math.sin(lat);

          X += x;
          Y += y;
          Z += z;
        });

        X = X / total;
        Y = Y / total;
        Z = Z / total;

        var Hyp = Math.sqrt(X * X + Y * Y);
        var circleConverter = 180 / Math.PI;
        
        return {
          lat: Math.atan2(Z, Hyp) * circleConverter,
          long: Math.atan2(Y, X) * circleConverter,
          latitude: Math.atan2(Z, Hyp) * circleConverter,
          longitude: Math.atan2(Y, X) * circleConverter
        };
      },

      calcZoomLevel: function(bounds, containerDims, maxZoomLevel) {
        var WORLD_DIM = { height: 256, width: 256 };

        function latRad(lat) {
          var sin = Math.sin(lat * Math.PI / 180);
          var radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
          return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
        }

        function zoom(mapPx, worldPx, fraction) {
          return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
        }

        var ne = bounds.getNorthEast();
        var sw = bounds.getSouthWest();

        var latFraction = (latRad(ne.lat()) - latRad(sw.lat())) / Math.PI;

        var lngDiff = ne.lng() - sw.lng();
        var lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

        var latZoom = zoom(containerDims.height, WORLD_DIM.height, latFraction);
        var lngZoom = zoom(containerDims.width, WORLD_DIM.width, lngFraction);

        return Math.min(latZoom, lngZoom, maxZoomLevel);
      },

      createBounds: function (geoPoints) {
        var bounds = new google.maps.LatLngBounds();

        _.each(geoPoints, function(geoPoint) {
          bounds.extend(new google.maps.LatLng(geoPoint.lat,geoPoint.long));
        });

        _map = null;
        return bounds;
      },

      createMapMarkers: function(mapEntity) {
        _.forIn(mapEntity.entities, function(record, index) {
          var geoPoint = record.geoPoint;
          mapEntity.mapMarkers[parseInt(index)] = new google.maps.Marker({
            position: new google.maps.LatLng(geoPoint.lat, geoPoint.long),
            icon: mapEntity.filepath,
            map: _map,
            animation: mapEntity.animation
          });
        });
      },

      extractGeoPoints: function() {
        // still takes arguments, despite not showing arguments

        var argumentCnt = arguments.length;
        var geoPointSets = new Array(argumentCnt);

        for (var index = 0; index < argumentCnt; index++) {
          geoPointSets[index] = _.pluck(arguments[index], 'geoPoint');
        }

        return _.flatten(geoPointSets);
      },

      _getIconEntities: function() {
        return _iconEntities;
      },

      _getBounds: function() {
        return _bounds;
      },
      
      getMap: function() {
        return _map;
      },

      getMaxZoomLevel: function(centerPoint) {
        var qMaxZoomLevel = $q.defer();
        var zoomService = new google.maps.MaxZoomService();
        var position = new google.maps.LatLng(centerPoint.latitude, centerPoint.longitude);

        zoomService.getMaxZoomAtLatLng(position, function(response) {
          if (response.status == google.maps.MaxZoomStatus.OK) {
              var maxZoom = 0;
              maxZoom = (_maxZoomLevel < response.zoom) ? _maxZoomLevel : response.zoom;
              qMaxZoomLevel.resolve(maxZoom);
          } else
              qMaxZoomLevel.reject({errMsg: "Couldn't get MaxZoomLevel"});
        });

        zoomService = null;
        return qMaxZoomLevel.promise;
      },

      prepMapParams: function(geoPoints, mapCenter, containerDims) {
        var self = this;
        var qMapSettings = $q.defer();
      
        _bounds = self.createBounds(geoPoints);
        this.getMaxZoomLevel(mapCenter).then(function(foundMaxZoom) {
          var dims = calcDims(containerDims)
          var zoomLevel = self.calcZoomLevel(_bounds,dims,foundMaxZoom);
          var mapSettings = {
            center: mapCenter,
            zoomLevel: zoomLevel,
            bounds: _bounds,
            dims: dims,
            options: {
              draggable: true,
              panControl: false,
              streetViewControl: false
            }
          };
          qMapSettings.resolve(mapSettings);
          geoPoints = center = element = null;
        });

        mapSettingsUpdated = true;
        return qMapSettings.promise;
      },

      // prepMarkerParams: function(objects, geoPointName, iconFilepathName, animationMethod) {
      //   return {
      //     models: objects,
      //     coords: geoPointName,
      //     options: {
      //       icon: iconFilepathName,
      //       animation: animationMethod
      //     }
      //   };
      // },

      // prepEntitiesForMap: function(model, data, idType) {
      //   return _.map(data, function(obj) {
      //     return {
      //       model: model,
      //       geoPoint: obj.geoPoint,
      //       id: idType
      //     };
      //   });
      // },

      setMaps: function(outsideMaps) {
        _map = outsideMaps;
      },

      wipeData: function() {
        _map = null;
      }
    };
  }])
  .factory("Nav", ['$filter','$q','LookupSet','Position','Profile','Request'
          ,'supersonic','Watcher',function($filter,$q,LookupSet,Position,Profile
          ,Request,supersonic,Watcher) {
    var _buttons, _buttonNames, _views;
    // var _preloadViews, _preloadViewNames;
    var _preloadViews = [], _qPreloadViews = [];
    var _subViews, _subViewNames;
    var _widgets, _widgetNames;

    return {
      buildMenuOptions: function() {
        return {
          controller: "btmSheetCtrl",
          disableParentScroll: true,
          templateUrl: "menu.html"
        }
      },

      buildOnTapOptions: function(channelName, destType, method, content) {
        return {
          channelName: channelName,
          destType: destType,
          method: method,
          params: {
            sender: this.parseViewName(steroids.view.location),
            content: content
          }
        }
      },

      modalOnTapOptions: function(targetSubView, navCb) {
        var modalOnTapOptions = this.buildOnTapOptions(
          "modalData"
          , "modal"
          , navCb
          , { targetSubView: targetSubView }
        );

        return modalOnTapOptions;
      },

      enterView: function(targetViewName, options) {
        var self = this;

        if (options.destType == "root" || options.destType == "initial") { // summary view
          options.method(options.animation);
          return;
        }

        this.getView(targetViewName).then(function(view) {
          // setup ONGOING listener for TowReq being ready
          var unsubscribe = supersonic.data.channel(targetViewName +"Ready").subscribe( function() {
            if (options.destType == "modal") {
              if ( angular.isDefined(options.animation) )
                options.method(view, options.animation);
              else
                options.method(view);
            } else if (options.destType == "pillar")
              options.method(view);

            unsubscribe();
          });

          var coreParams = {
            sender: self.parseViewName(steroids.view.location),
            content: {
              myEmployerId: Profile._getParam("myEmployerId"),
              myOrgTypeId: Profile._getParam("myOrgTypeId"),
              geoPoint: Position.getGeoPoint(),
              lookupSets: LookupSet.buildLookupSets()
            }
          };

          if (options.publish != null)
            options.publish();

          _qPreloadViews[targetViewName].promise.then(function() {
            supersonic.data.channel("coreData").publish(coreParams);
            supersonic.data.channel(options.channelName).publish(options.params);
            // if (actionCb.options.methodType == "modal")
              // supersonic.data.channel("modalReady").publish(options.params);  
            // else if (actionCb.options.methodType == "push")
              // supersonic.data.channel(viewName +"Data").publish(options.params);  
          });
        });
      },

      exitView: function(viewName, actionCb) {
        _.forIn(Watcher._getUnWatchers(), function(unwatcher, index) {
          unwatcher();
        });

        Watcher.resetWatchers();
        Watcher.runUnWatchers();
        actionCb();
      },

      getButton: function(btnName) {
        return _.findWhere(_buttons, {"name": btnName});
      },

      getButtons: function() {
        return _buttons;
      },

      _getPreloadViews: function() {
        return _preloadViews;
      },

      _getQPreloadView: function(viewName) {
        return _qPreloadViews[viewName];
      },

      _getSubViews: function() {
        return _subViews;
      },

      _getWidgets: function() {
        return _widgets;
      },

      getView: function(viewName) {
        var qView = $q.defer();

        supersonic.ui.views.find(this.locationify(viewName)).then(function(foundView) {
          qView.resolve(foundView);
        }, function() {
          // no view found
          var _view = _.findWhere(_views, {id: viewName});
        
          if (!_view)
            _view = _views[viewName] = this.setupView(viewName);
          qView.resolve(view);
        });

        return qView.promise;
      },

      initButtons: function(btnName, filename, side, rank, setupCb) {
        return {
          filename: filename,
          name: btnName,
          side: side,
          rank: rank,
          navBtn: setupCb(btnName, filename)
        };
      },

      // start views
      initPreloadView: function(preloadView) {
        _preloadViews.push(preloadView);
      },

      initSubViews: function(subViews) {
        _subViews = new Array(subViews.length);
        _subViewNames = subViews;

        _.forIn(subViews, function(subView, key) {
          _subViews[subView] = false;
        });
      },

      initWidgets: function(widgets) {
        _widgets = new Array(widgets.length);
        _widgetNames = widgets;
        _.forIn(widgets, function(subView, key) {
          _widgets[subView] = false;
        });
      },

      leaveModal: function(navCb) {
        this.resetSubViews();
        this.exitView(this.thisView(), navCb);
      },

      locationify: function(viewName) {
        return viewName +"#" +viewName;
      },

      logout: function() {
        // broadcast message to unbind
        // broadcast message to destroy all objects
        // broadcats message to remove listeners
        // broadcast message to unwatch watchers
        var authRef = Profile._getParam("authRef");

        authRef.$unauth();
        this.enterView("auth", {
          animation: supersonic.ui.animate("flipHorizontalFromRight", { duration: 0.3 }),
          destType: "initial",
          method: supersonic.ui.initialView.show
        });
      },

      parseViewName: function(viewUrl) {
        var beg = viewUrl.lastIndexOf('/') +1;
        var end = viewUrl.lastIndexOf('.');
        return viewUrl.slice(beg, end);
      },

      removeView: function(actionCb) {
        actionCb();
      },

      resetSubViews: function(subViews) {
        _.each(_subViewNames, function(subViewName) {
          _subViews[subViewName] = false;
        });
      },

      resetWidgets: function(widgetNames) {
        _.each(widgetNames, function(widgetName) {
          _widgets[widgetName] = false;
        });
      },

      setButtons: function(buttons) {
        _buttons = buttons;
        _buttonNmaes = buttons;
      },

      setButton: function(button) {
        _buttons.push(button);
      },

      setupButton: function(btnName, filename) {
        var navBtn = new steroids.buttons.NavigationBarButton();
        navBtn.imagePath = "/icons/" +filename;
        // navBtn.styleClass = "super-navbar-button";
        navBtn.styleClass = "button-icon super-ios7-checkmark";
        return navBtn;
      },

      setupView: function(viewName) {
        return new supersonic.ui.View({
          location: this.locationify(viewName),
          id: viewName
        });
      },

      startView: function(preloadView) {
        _qPreloadViews[preloadView] = $q.defer();

        this.getView(preloadView).then(function(view) {
          view.isStarted().then(function(startStatus) {
            if (!startStatus) {
              view.start();
              var unsubscribeDOMReady = supersonic.data.channel("DOMReady").subscribe( function() {
                unsubscribeDOMReady();
                _qPreloadViews[preloadView].resolve();
              });
            } else {
              _qPreloadViews[preloadView].resolve();
            }
          });
        });
      },

      switchSubView: function(subViewName) {
        this.resetSubViews(_subViews);
        _subViews[subViewName] = true;
      },

      switchWidget: function(widgetName) {
        this.resetWidgets(_widgetNames);
        _widgets[widgetName] = true;
      },

      thisView: function() {
        return this.parseViewName(steroids.view.location);
      },

      toggleSubView: function(subViews) {
        var nextSubView = "";
        _.forIn(subViews, function(state, subViewName) {
          if (state == false) {
            nextSubView = subViewName;
          }
        })

        this.switchSubView(nextSubView);
      },

      toggleWidget: function(widgets) {
        var nextWidget = "";
        _.forIn(_widgets, function(state, WidgetName) {
          if (state == false) {
            nextWidget = widgetName;
          }
        })

        this.switchWidget(nextWidget);
      },
      
      destroy: function() {
        _.each(_views, function(view) {
          view = null;
        });
      }
    }
  }])
  .factory('Position', ['$q', function($q) {
    var _currentGeoPoint = {
      lat: 0.0,
      long: 0.0,
      latitude: 0.0,
      longitude: 0.0,
      timestamp: moment().format()
    };

    // var _stopWatcherCb = null;

    return {
      calcDistance: function(beg, end) {
        var begX = beg.lat, endX = end.lat;
        var begY = beg.long, endY = end.long;
        return Math.sqrt(Math.pow(endX - begX, 2) + Math.pow(endY - begY, 2));
      },
      getGeoPoint: function() {
        // var qGeoPoint = $q.defer();

        // supersonic.device.geolocation.getPosition().then(function(position) {
        //   var geoPoint = {
        //     lat: position.coords.latitude,
        //     long: position.coords.longitude,
        //     latitude: position.coords.latitude,
        //     longitude: position.coords.longitude
        //   };

        //   qGeoPoint.resolve(geoPoint);
        // }, function(errResults) {
        //    console.log(errResults);
        // }, {
        //    enableHighAccuracy: false // CREATE A PROVIDER
        // });

        // return qGeoPoint.promise;
        return _currentGeoPoint;
      },

      _setGeoPoint: function(newGeoPoint) {
        _currentGeoPoint = newGeoPoint;
      },

      getDistances: function(origin, targetDestinations, callback) {
        var mapOrigin = new google.maps.LatLng(origin.lat, origin.long);
        var distanceService = new google.maps.DistanceMatrixService();

        distanceService.getDistanceMatrix({
        origins: [mapOrigin],
        destinations: _.map(targetDestinations, function(dest) {
                        return new google.maps.LatLng(dest.lat, dest.long);
                     }),
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.IMPERIAL,
        avoidHighways: false,
        avoidTolls: false
        }, callback);
      },

      prepForDistances: function(requests, newGeoPoint) {
        var qDistances = $q.defer();
        
        if (!requests || requests.length < 1)
          qDistances.reject("No requests exist");
        else {
          var requestGeoPoints = _.pluck(requests, "geoPoint");
          this.getDistances(newGeoPoint,requestGeoPoints,function(resp,status) {
            qDistances.resolve(resp); // add error || exception handling
          });  
        }

        return qDistances.promise;
      }
    };
  }])
  .factory('Profile', ['$q','$filter','$firebaseArray','$firebaseObject','$firebaseAuth'
                    ,'$rootScope','DB','ORGTYPE_CONST',function($q,$filter,$firebaseArray
                    , $firebaseObject,$firebaseAuth,$rootScope,DB,ORGTYPE_CONST) {
    var _params = new Array(10);
    _params["authRef"] = $firebaseAuth(DB.getRef());
    _params["rememberMe"] = null;
    _params["myOrgTypeId"] = null;
    _params["myEmployerId"] = null;
    _params["userInfo"] = null;
    _params["sites"] = new Array(2); // 0 = vendors, 1 = clients
    _params["colors"] = ['calm', 'assertive'];

    function _retrieveSites(siteIds, orgTypeId) {
      var qSites = $q.defer();
      var orgTypeName = profileService.getOrgTypeName(orgTypeId);

      // $firebase( DB.getRef().child(orgTypeName) ).$asArray().$loaded()
      $firebaseArray( DB.getRef().child(orgTypeName) ).$loaded().then(function(results) {
        var sites = _.filter(results, function(site) {
          return _.contains(siteIds, site.$id)
        })

        results = null;
        qSites.resolve(sites);
      });

      return qSites.promise;
    }

    var profileService = {
      _addSite: function(orgTypeId, site) {
        _params["sites"][orgTypeId].push(site);
      },

      _getParam: function(param) {
        return _params[param];
      },

      _setParam: function(param, data) {
        _params[param] = data;
      },

      _getSites: function(orgTypeId) {
        return _params["sites"][orgTypeId];
      },
      
      _setSites: function(orgTypeId, data) {
        _params["sites"][orgTypeId] = data;
      },

      _themeColors: function(myOrgTypeId) {
        return _params["themeColors"][myOrgTypeId];
      },

      findActiveAccessRights: function(userInfo) {
        // find and set all partyIds
        var activeAccessRights = _.where(userInfo.accessRights, {'isActive': true});
        return activeAccessRights;
      },

      getFbUserInfo: function(fbUserRef) {
        return fbUserRef.$asObject().$loaded();
      },

      getOrgTypeName: function(orgTypeId) {
        var orgTypeName = $filter('orgType')(orgTypeId, "name").toLowerCase();
        return orgTypeName;
      },

      retrieveIds: function(objects, idKey) {
        return _.uniq( _.pluck(objects, idKey) );
      },

      retrieveCoreIds: function(userInfo) {
        var activeAccessRights = this.findActiveAccessRights(userInfo);
        var activeOrgTypeIds = this.retrieveIds(activeAccessRights, 'orgTypeId');
        $rootScope.myOrgTypeId = $rootScope.setOrgTypeId(activeOrgTypeIds);
        $rootScope.myEmployerId = _params["myEmployerId"];
      },

      retrieveSite: function(siteId, orgTypeName) {
        var fbSiteRef = DB.getRef().child(orgTypeName).child(siteId);
        return $firebaseObject(fbSiteRef).$loaded();
      },

      retrieveSites: function(siteIds, orgTypeId) {
        /*
        ** Need to redo this function!!!
        */
        var qSites = $q.defer();
        var orgTypeName = this.getOrgTypeName(orgTypeId);

        $firebaseArray( DB.getRef().child(orgTypeName) ).$loaded()
          .then(function(results) {
            var sites = _.filter(results, function(site) {
              return _.contains(siteIds, site.iid);
            });

            results = null;
            qSites.resolve(sites);
          });

        return qSites.promise;
      },

      setupRequiredSites: function(myOrgTypeId, myEmployerId) {
        // WHO DO I WORK FOR
        // WHO ARE MY CLIENTS

        var self = this;
        var qSites = new Array(2); // vendor + client
        var siteIdsByMyOrgType;
        // var myEmployerId = _params["myEmployerId"];
        // var myOrgTypeId = _params["myOrgTypeId"];
        var constituentOrgTypeId = this.toggleOrgTypeId(myOrgTypeId).toString();
        qSites[ORGTYPE_CONST.VENDOR] = $q.defer(), qSites[ORGTYPE_CONST.CLIENT] = $q.defer();

        if (myEmployerId) {
          siteIdsByMyOrgType = [myEmployerId];
        } else {
          // accessRights used to extrapolate siteIds of employer
          var activeARs = this.findActiveAccessRights(_params["userInfo"]);
          var activeARsByMyOrgType = _.where(activeARs, {orgTypeId: myOrgTypeId});
          siteIdsByMyOrgType = this.retrieveIds(activeARsByMyOrgType, 'siteId');  
        }
        
        // retrieve sites of myOrgType (employer)
        this.retrieveSites(siteIdsByMyOrgType, myOrgTypeId)
          .then(function(employerSites) {
            self._setSites(myOrgTypeId, employerSites);
            return employerSites;
          })
          .then(function(employerSites) {
            // auto-select 1st employer site
            var myEmployerSite = _.first(employerSites);
            
            if (!$rootScope.myEmployerId) {  
              self._setParam("myEmployerId", myEmployerSite.$id);
              _params["userInfo"].state.myEmployerId = myEmployerSite.$id;
              _params["userInfo"].$save();
            }
            
            qSites[myOrgTypeId].resolve(myEmployerSite);
          });
        
        qSites[myOrgTypeId].promise.then(function(myEmployerSite) {
          var constituentOrgTypeName = self.getOrgTypeName(constituentOrgTypeId);
          var constituentSiteRefs = myEmployerSite[constituentOrgTypeName]; // don't filter 'inactive' sites just yet
          var constituentSiteIds = self.retrieveIds(constituentSiteRefs, 'id');
          
          self.retrieveSites(constituentSiteIds, constituentOrgTypeId)
            .then(function(constituentSites) {
              self._setSites(constituentOrgTypeId, constituentSites);
              return constituentSites;
            })
            .then(function(constituentSites) {
              self._setSites(constituentOrgTypeId, constituentSites);
              qSites[constituentOrgTypeId].resolve(constituentSites);
            });
        });
        
        return qSites[constituentOrgTypeId].promise;
      },

      substrUid: function(uid) {
        return uid.substr(uid.lastIndexOf(':') + 1);
      },

      toggleOrgTypeId: function(orgTypeId) {
        return Math.abs(parseInt(orgTypeId) - 1).toString();
      },

      destroy: function() {
        _params["authRef"] = null;
        _params["rememberMe"] = null;
        _params["userInfo"].$destroy();
        _params["userInfo"] = null;
        _.each(_params["sites"], function(site) {
          site = null;
        });
      }
    };

    return profileService;
  }])
  .factory("Request", ['$firebaseArray','$firebaseObject','$q','$rootScope','DB'
                    ,'LookupSet','ORGTYPE_CONST','Profile','Util',function(
                    $firebaseArray,$firebaseObject,$q,$rootScope,DB,LookupSet
                    ,ORGTYPE_CONST,Profile,Util) {
    var _priorYearCount = 30;
    var _chosenEventId;
    var _requests = [], _request;
    var _requiredPicTypeIds;
    var _rootRef = DB.getRef();
    var _stagedPics;
    var _statusEntries;
    var xRequests;

    return {
      _getPriorYearCount: function() {
        return _priorYearCount;
      },

      _getRequest: function(requestId) {
        if (arguments.length == 0) return _request;
        else return _.findWhere(_requests, {$id: requestId});
      },

      _getRequests: function(requestIds) {
        if (arguments > 0) {
          return _.filter(_requests, function(request) {
            return _.contains(requestIds, requests.$id);
          });
        } else
          return _requests;
      },

      _getRequiredPicTypeIds: function() {
        return _requiredPicTypeIds;
      },

      _getStagedPic: function(picTypeId) {
        return _.findWhere(_stagedPics, {iid: picTypeId});
      },

      _getStagedPics: function() {
        return _stagedPics;
      },

      _getStatusEntries: function() {
        return _statusEntries;
      },

      _addRequest: function(request) {
        _requests.push(request);
      },

      _setPriorYearCount: function(count) {
        _priorYearCount = count;
      },

      _setRequest: function(request) {
        _request = null;
        _request = request;
      },

      _setRequests: function(requests) {
        _requests = requests;
      },

      _setRequiredPicTypeIds: function(picTypeIds) {
        _requiredPicTypeIds = picTypeIds;
      },

      _setStagedPics: function(picTypes) {
        _stagedPics = picTypes;
      },

      _setStatus: function(newStatusEntry) {
        _request.statuses.push(newStatusEntry);
        return _request.$save();
      },

      _setStatusEntries: function(statusEntries) {
        _statusEntries = statusEntries;
      },

      assignToSite: function(site, requestId) {
        if (angular.isDefined(site.requests)) {
          if (Array.isArray(site.requests) ) {
            if (!_.contains(site.requests, requestId) ) {  
              site.requests.push(requestId);
            } else
              return;
          } else
            site.requests = [requestId];
        } else {
          angular.extend(site, {
            requests: [requestId]
          });
        }
          
        site.$save();
      },

      buildChildTemplate: function(data) {
        var template = DB.getChildTemplate("request", "status");
        _.forIn(data, function(value, key) {
          template[key] = value;
        })

        return template;
        // {
        //   template.timestamp = data.timestamp;
        //   geoPoint: {
        //     lat: 0.0,
        //     long: 0.0,
        //     latitude: 0.0,
        //     longitude: 0.0
        //   },
        //   statusId: "",
        //   author: {
        //     id: "",
        //     orgTypeId: ""
        //   },
        //   notes: "",
        //   signFilename: ""
        // };
      },

      convertVehicleData: function(vCopy, foundVehicle) {
        vCopy.drivetrain.id = foundVehicle.drivetrainId;
        vCopy.make = foundVehicle.make;
        vCopy.model = foundVehicle.model;
        vCopy.year = foundVehicle.year;
        vCopy.transmission.id = foundVehicle.transTypeId;
        vCopy.vehicleType.id = foundVehicle.vehicleTypeId;

        // vCopy.vin = foundVehicle.vin;
        // vCopy.make = foundVehicle.make.name;
        // vCopy.model = foundVehicle.model.name;
        // vCopy.year = _.first(foundVehicle.years).year;
        
        // var transType = Model.getRecordByParam('transTypes', 'name', foundVehicle.transmission.transmissionType);
        // vCopy.transTypeId = transType.id;

        // var vehicleType = Model.getRecordByParam('vehicleTypes', 'name', foundVehicle.categories.vehicleType);
        // vCopy.vehicleTypeId = vehicleType.id;

        return vCopy;
      },

      isStatusLocked: function(status) {
        return status.lockForUser;
      },

      checkOrgTypeRights: function(statusLookup) {
        return statusLookup.userRights[$rootScope.myOrgTypeId].write;
      },

      assignedToRequest: function(userInfo, statusLookup) {
        var assignmentStatus;

        switch($rootScope.myOrgTypeId) {
          case ORGTYPE_CONST.VENDOR:
            assignmentStatus = (userInfo.state.requestId && userInfo.state.requestId != "");
            break;
          case ORGTYPE_CONST.CLIENT:
            assignmentStatus = false;
            break;
        }

        return assignmentStatus;
      },

      assignedToThisRequest: function(userInfo) {
        var assignmentStatus;
        var request = _request;

        switch($rootScope.myOrgTypeId) {
          case ORGTYPE_CONST.VENDOR:
            assignmentStatus = (userInfo.state.requestId &&
                              userInfo.state.requestId == request.$id);
            break;
          case ORGTYPE_CONST.CLIENT:
            assignmentStatus = true;
            break;
        }

        return assignmentStatus;
      },

      validatePermissions: function(nextStatusRef) {
        var statusLookup = LookupSet._getLookupSet("statuses", nextStatusRef.iid);
        var userInfo = Profile._getParam("userInfo");
        var allowed = false;

        if (this.checkOrgTypeRights(statusLookup) == true) {
          // check lock status
          if (this.isStatusLocked(statusLookup))
            allowed = this.assignedToThisRequest(userInfo) ? true : false;
          else
            allowed = this.assignedToRequest(userInfo) &&
                      Array.isArray(statusLookup.nextStatuses)
                      ? false : true;
        } else {
          allowed = false;
        }

        return allowed ? statusLookup : undefined;
      },

      extractNextStatuses: function(lastStatusEntry) {
        var self = this;
        var lastStatusLookup = LookupSet._getLookupSet("statuses", lastStatusEntry.statusId);
        
        if ( !angular.isDefined(lastStatusLookup.nextStatuses)
            || !Array.isArray(lastStatusLookup.nextStatuses)
            || lastStatusLookup.nextStatuses.legnth < 1 )
          return undefined;

        var nextStatuses = _.transform(lastStatusLookup.nextStatuses, function(result, nextStatusRef) {
          var nextStatus = self.validatePermissions(nextStatusRef);
          
          if ( angular.isDefined(nextStatus) || nextStatus != null)
            return result.push(nextStatus);
        });
        
        return nextStatuses;
      },

      generateImgId: function(timestamp,userId,clientId,orgTypeId,picTypeId,fileType) {
        return timestamp +"-" +userId +"-" +clientId +"-" +orgTypeId +"-" +picTypeId +"." +fileType;
      },

      prepDbChild: function(dBChild) {
        return _dBChilds[dBChild];
      },

      publishRequest: function(newRequest) {
        var requestsRef = _rootRef.child("requests");
        return requestsRef.push(newRequest, function(err) {
          console.log(err ? err : "Successfully Set");
        });
      },

      retrieveRequest: function(reqId, childParam) {
        var request = this._getRequest();
        var qRequest = $q.defer();
        
        if ( request && (request.$id == reqId) ) {
          qRequest.resolve(request);
          return qRequest.promise;
        } else {
          // var requestsRef = _rootRef.child("requests");
          var dbQuery = "requests/" +reqId;
          if (arguments[1])
            dbQuery = dbQuery +"/" +childParam;

          return DB.getDbRecord(dbQuery);
        }
      },

      setupRequests: function(myOrgTypeId) {
        // get list of all site Ids
        var myOrgTypeName = Profile.getOrgTypeName(myOrgTypeId);
        var employerSites = Profile._getSites(myOrgTypeId);
        var employerSite = _.findWhere(employerSites, {'$id': myEmployerId});
        var myEmployerId = Profile._getParam("myEmployerId");

        var employerRef = _rootRef.child(myOrgTypeName);
        var reqIdListRef = employerRef.child( Util.convertId(myEmployerId) ).child("requests");
        var requestsRef = _rootRef.child("requests");
        // var qRequests, i;
        var qRequests = $q.defer();
        var cnt = 0;

        reqIdListRef.once('value', function(reqIdList) {
          var requestCount = reqIdList.val().length;

          reqIdListRef.on('child_added', function(reqChild) {
            requestsRef.child(reqChild.val()).once("value", function(request) {
              $firebaseObject(request.ref()).$loaded().then(function(req) {
                _requests.push(req);
                if (++cnt >= requestCount)
                  qRequests.resolve(_requests);
              });
            }, function(error) {
              console.log(error);
            });
          });
        });
        return qRequests.promise;
      },

      destroy: function() {
        _.each(_requests, function(request) {
          request.$destroy;
          request = null;
        });
      }
    }
  }])
  .factory('User', ['$firebaseArray','$firebaseObject','$q','DB','Profile'
                ,function($firebaseArray,$firebaseObject,$q,DB,Profile) {
    var _employerUsers = [], _constituentUsers = [], _loginUsers = [];

    return {
      setLoginUsers: function(users) {
        _employerUsers = users;
      },
      getEmployerUsers: function(userId) {
        if (!userId || userId == "")
          return _employerUsers;
        else
          return _.find(_employerUsers, {$id: userId});
      },
      setEmployerUsers: function(users) {
        _employerUsers = users;
      },
      setupEmployerUsers: function(myEmployerSite, myOrgTypeId) {
        // get list of all site Ids
        var myOrgTypeName = Profile.getOrgTypeName(myOrgTypeId);
        var rootRef = DB.getRef();
        var employerRef = rootRef.child(myOrgTypeName).child(myEmployerSite.$id);
        var employerUsersRef = employerRef.child('users');
        var usersRef = rootRef.child('users');
        var qEmployerUsers = $q.defer();
        var cnt = 0;

        // employerUsersRef.on('value', function(employerUsers) {
        //   var userCount = employerUsers.val().length;
        //   _employerUsers = new Array(userCount);

        //   for (var i = 0; i < qEmployerUsers.length; i++) {
        //     qEmployerUsers[i] = $q.defer();
        //   }

        //   var cnt = 0;
        //   employerUsersRef.on('child_added', function(userRef) {
        //     usersRef.child(userRef.val().id).once("value", function(userRef) {
        //       $firebaseObject(userRef.ref()).$loaded().then(function(user) {
        //         qEmployerUsers[cnt].resolve(user);
        //         cnt++;
        //       });
        //     }, function(error) {
        //       console.log(error);
        //     });
        //   });
        // });



        employerUsersRef.once('value', function(userIdList) {
          var userCount = userIdList.val().length;

          employerUsersRef.on('child_added', function(userIdChild) {
            usersRef.child(userIdChild.val().id).once("value", function(userChild) {
              $firebaseObject(userChild.ref()).$loaded().then(function(employerUser) {
                _employerUsers.push(employerUser);
                if (++cnt >= userCount)
                  qEmployerUsers.resolve(_employerUsers);
              });
            }, function(error) {
              console.log(error);
            });
          });
        });

        return qEmployerUsers.promise;
      },
      getConstituentUser: function(userId) {
        if (userId >= 0)
          return _.find(_constituentUsers, {$id: userId});
        else
          return _constituentUsers;
      },
      addConstituentUser: function(user) {
        _constituentUsers.push(user);
      },
      setupConstituentUser: function(userId) {
        var qConstituentUser = $q.defer();
        var rootRef = DB.getRef();
        $firebaseObject(rootRef.child('users').child(userId)).$loaded().then(function(user) {
          qConstituentUser.resolve(user);
        });

        return qConstituentUser.promise;
      },
      destroy: function() {
        _.each(_employerUsers, function(user) {
          user = null;
        });
        
        _.each(_constituentUsers, function(user) {
          user = null;
        });
        
        _.each(_loginUsers, function(user) {
          user = null;
        });
      }
    };
  }])
  .factory('Util', function() {
    return {
      changeToNull: function(records, targetParam) {
        _.each(records, function(record) {
          if (record[targetParam] == "")
            record[targetParam] = null;
        });
      },

      compareIntValues: function(a, b) {
        return (parseInt(a) === parseInt(b)) ? true : false;
      },

      convertId: function(id) {
        var intId = parseInt(id);
        return (typeof intId === "number") ? intId : id;
      },

      getObjLength: function(obj) {
        return parseInt(obj.length);
      },

      trimString: function(slug, unwantedLetter) {
        return slug.substr(0, slug.lastIndexOf(unwantedLetter));
      },

      valueToUpper: function(obj) {
        return obj.toUpperCase(obj);
      },
    }
  })

angular.module('TestData', [])
  .factory('SitesData', function() {
    var _sites = [
      [
        {
          "iid": "0",
          "name": "Gonzalez Towing",
          "filename": "0.jpg"
        }, {
          "iid": "1",
          "name": "Speed of Light Towing",
          "filename": "1.jpg"
        }, {
          "iid": "2",
          "name": "Tri-county Towing",
          "filename": "2.png"
        }, {
          "iid": "3",
          "name": "A-1 Towing",
          "filename": "3.jpg"
        }
      ], [
        {
          "iid": "0",
          "name": "Live Oak Supermarket",
          "filename": "0.jpg"
        }, {
          "iid": "1",
          "name": "The Crows Nest",
          "filename": "1.jpg"
        }, {
          "iid": "2",
          "name": "Rainbow Carpet One",
          "filename": "2.jpg"
        }, {
          "iid": "3",
          "name": "Starbucks Coffee",
          "filename": "3.jpg"
        }, {
          "iid": "4",
          "name": "Capitola Laundrymat",
          "filename": "no-site.jpg"
        }
      ]
    ];
    
    return {
      getSites: function() {
        return _sites;
      },
      getSitesByOrgType: function(orgTypeId) {
        // return _.findWhere(_sites[orgTypeId], {iid: siteId});
        return _sites[orgTypeId];
      }
    };
  })
  .factory('UserData', ['$firebaseArray','$firebaseObject','$q','DB',function($firebaseArray
                      ,$firebaseObject,$q,DB) {
    // var _users;

    // function retrieveUsers() {
    //   var usersRef = DB.getRef().child('users');

    //   return $firebase(usersRef).$asArray().$loaded().then(function(fbUsers) {
    //     return _users = fbUsers;
    //   });
    // }

    var _users = [
      [
        {
          uid: "3",
          email: 'bgonzalez@gonzalez-towing.com',
          password: 'bgonzalez',
          name: {
            first: 'Brad',
            last: 'Gonzalez'
          },
          filename: "bgonzalez.jpg",
          orgTypeId: "0",
          siteId: "0"
        }, {
          uid: "4",
          email: 'tgonzalez@gonzalez-towing.com',
          password: 'tgonzalez',
          name: {
            first: 'Tony',
            last: 'Gonzalez'
          },
          filename: "tgonzalez.jpg",
          orgTypeId: "0",
          siteId: "0"
        }, {
          uid: "5",
          email: 'jgonzalez@gonzalez-towing.com',
          password: 'jgonzalez',
          name: {
            first: 'Juan',
            last: 'Gonzalez'
          },
          filename: "jgonzalez.jpg",
          orgTypeId: "0",
          siteId: "0"
        }, {
          uid: "6",
          email: 'ggonzalez@gonzalez-towing.com',
          password: 'ggonzalez',
          name: {
            first: 'Gary',
            last: 'Gonzalez'
          },
          filename: "ggonzalez.jpg",
          orgTypeId: "0",
          siteId: "0"
        }, {
          uid: "7",
          email: 'ygonzalez@gonzalez-towing.com',
          password: 'ygonzalez',
          name: {
            first: 'Ynes',
            last: 'Gonzalez'
          },
          filename: "ygonzalez.jpg",
          orgTypeId: "0",
          siteId: "0"
        }, {
          uid: "9",
          email: 'afoley@speedoflighttowing.com',
          password: 'afoley',
          name: {
            first: 'Axel',
            last: 'Foley'
          },
          filename: "afoley.jpg",
          orgTypeId: "0",
          siteId: "1"
        }, {
          uid: "12",
          email: 'gbettencourt@tricountytowing.com',
          password: 'gbettencourt',
          name: {
            first: 'Gino',
            last: 'Bettencourt'
          },
          filename: "gbettencourt.jpg",
          orgTypeId: "0",
          siteId: "2"
        }, {
          uid: "13",
          email: 'mramirez@tricountytowing.com',
          password: 'mramirez',
          name: {
            first: 'Manny',
            last: 'Ramirez'
          },
          filename: "mramirez.jpg",
          orgTypeId: "0",
          siteId: "2"
        }, {
          uid: "20",
          email: 'sconnery@louraltowing.net',
          password: 'sconnery',
          name: {
            first: 'Sean',
            last: 'Connery'
          },
          filename: "sconnery.jpg",
          orgTypeId: "0",
          siteId: "3"
        }, {
          uid: "21",
          email: 'jtravolta@louraltowing.net',
          password: 'jtravolta',
          name: {
            first: 'John',
            last: 'Travolta'
          },
          filename: "jtravolta.jpg",
          orgTypeId: "0",
          siteId: "3"
        }, {
          uid: "22",
          email: 'gclooney@louraltowing.net',
          password: 'gclooney',
          name: {
            first: 'George',
            last: 'Clooney'
          },
          filename: "gclooney.jpg",
          orgTypeId: "0",
          siteId: "3"
        }
      ], [
        {
          uid: "1",
          email: 'jchang@liveoaksuper.com',
          password: 'jchang',
          name: {
            first: 'Jerry',
            last: 'Chang'
          },
          filename: "jchang.jpg",
          orgTypeId: "1",
          siteId: "0"
        }, {
          uid: "2",
          email: 'acrabtree@liveoaksuper.com',
          password: 'acrabtree',
          name: {
            first: 'Adam',
            last: 'Crabtree'
          },
          filename: "acrabtree.jpg",
          orgTypeId: "1",
          siteId: "0"
        }, {
          uid: "8",
          email: 'mfoy@rainbowcarpetonesantacruz.com',
          password: 'mfoy',
          name: {
            first: 'Mike',
            last: 'Foy'
          },
          filename: "mfoy.jpg",
          orgTypeId: "1",
          siteId: "2"
        }, {
          uid: "10",
          email: 'krafferty@crowsnest-santacruz.com',
          password: 'krafferty',
          name: {
            first: 'Kevin',
            last: 'Rafferty'
          },
          filename: "krafferty.jpg",
          orgTypeId: "1",
          siteId: "1"
        }, {
          uid: "11",
          email: 'achang28@yahoo.com',
          password: 'achang28',
          name: {
            first: 'Albert',
            last: 'Chang'
          },
          filename: "achang28.jpg",
          orgTypeId: "1",
          siteId: "3"
        }, {
          uid: "14",
          email: "jmarsh@liveoaksuper.com",
          password: 'jmarsh',
          name: {
            first: 'Jeff',
            last: 'Marsh'
          },
          filename: "jmarsh.jpg",
          orgTypeId: "1",
          siteId: "0"
        }, {
          uid: "15",
          email: "mtucker@liveoaksuper.com",
          password: 'mtucker',
          name: {
            first: 'Matt',
            last: 'Tucker'
          },
          filename: "mtucker.jpg",
          orgTypeId: "1",
          siteId: "0"
        }, {
          uid: "16",
          email: "jlachlan@capitolalaundry.com",
          password: 'jlachlan',
          name: {
            first: 'Jonathon',
            last: 'Lachlan-Haché'
          },
          filename: "jlachlan.jpg",
          orgTypeId: "1",
          siteId: "4"
        }, {
          uid: "17",
          email: "mtsai@capitolalaundry.com",
          password: 'mtsai',
          name: {
            first: 'Michael',
            last: 'Tsai'
          },
          filename: "mtsai.jpg",
          orgTypeId: "1",
          siteId: "4"
        }, {
          uid: "18",
          email: "skyle@capitolalaundry.com",
          password: 'skyle',
          name: {
            first: 'Scott',
            last: 'Kyle'
          },
          filename: "skyle.jpg",
          orgTypeId: "1",
          siteId: "4"
        }, {
          uid: "19",
          email: "ssturmer@capitolalaundry.com",
          password: 'ssturmer',
          name: {
            first: 'Simon',
            last: 'Sturmer'
          },
          filename: "ssturmer.jpg",
          orgTypeId: "1",
          siteId: "4"
        }
      ]
    ];
    
    return {
    //   // getUsers: function(userId) {
      getUsers: function(orgTypeId) {
        var qUsers = $q.defer();

    //     // return userId ? _.findWhere(_users, {uid: userId}) : _users;
        if (!_users)
          retrieveUsers().then(function(users) {
            qUsers.resolve(_users = users);
          });
        else
          qUsers.resolve(_users);

        return qUsers.promise.then(function(users) {
          if (!orgTypeId)
            return users;
          else
            return _.findWhere(_users, {"orgTypeId": orgTypeId});
        })
      }
    };
  }])
  .factory("Watcher", function() {
    var _watchers = [], _unwatchers = [];

    return {
      // addWatcherToStart: function(watcher) {
      addToWatchers: function(watcher) {
        _watchers.push(watcher);
      },

      _getUnWatchers: function() {
        return _unwatchers;
      },

      // _setUnWatcher: function(unwatcher) {
      _addToUnwatchers: function(unwatcher) {
        _unwatchers.push(unwatcher);
      },

      resetWatchers: function() {
        _.each(_watchers, function(_watcher) {
          _watcher();
        });

        _watchers = null;
        _watchers = [];
      },

      runUnWatchers: function() {
        _.each(_unwatchers, function(_unWatcher) {
          _unWatcher();
        });

        _unwatchers = null;
        _unwatchers = [];
      }
    }
  })
  .factory('VehicleData', function() {
    var _vehicles = [
      {
        drivetrainId: "0",
        make: "Honda",
        model: "Civic",
        transTypeId: "0",
        vehicleTypeId: "0",
        vin: "1HGFA16857L135270",
        year: 2007
      }, {
        drivetrainId: "1",
        make: "BMW",
        model: "318ti",
        transTypeId: "1",
        vehicleTypeId: "0",
        vin: "wbacg7325vas99191",
        year: 1997
      }, {
        drivetrainId: "0",
        make: "Ford",
        model: "Fusion",
        transTypeId: "0",
        vehicleTypeId: "0",
        vin: "3FA6P0K98DR325797",
        year: 2013
      }, {
        drivetrainId: "1",
        make: "BMW",
        model: "Z4",
        transTypeId: "1",
        vehicleTypeId: "0",
        vin: "4usbt33423lr61390",
        year: 2003
      }, {
        drivetrainId: "1",
        make: "Mini",
        model: "Cooper",
        transTypeId: "1",
        vehicleTypeId: "0",
        vin: "wmwmf33559tz21048",
        year: 2009
      }, {
        drivetrainId: "1",
        make: "Yamaha",
        model: "Z257",
        transTypeId: "1",
        vehicleTypeId: "5",
        vin: "jyarn23y69a000089",
        year: 2013
      }, {
        drivetrainId: "1",
        make: "Toyota",
        model: "Tacoma",
        transTypeId: "0",
        vehicleTypeId: "1",
        vin: "4tanl42n4xz519793",
        year: 1999
      }, {
        drivetrainId: "0",
        make: "Toyota",
        model: "Prius",
        transTypeId: "0",
        vehicleTypeId: "0",
        vin: "jtdkb20ux53110464",
        year: 2005
      }, {
        drivetrainId: "0",
        make: "Chevrolet",
        model: "Spark",
        transTypeId: "0",
        vehicleTypeId: "0",
        vin: "KL8CD6S91EC534865",
        year: 2014
      }, {
        drivetrainId: "1",
        make: "Volkswagon",
        model: "GTI",
        transTypeId: "1",
        vehicleTypeId: "0",
        vin: "9BWKE61J334068557",
        year: 2003
      }, {
        drivetrainId: "2",
        make: "Acura",
        model: "MDX",
        transTypeId: "0",
        vehicleTypeId: "2",
        vin: "2HNYD18262H512079",
        year: 2002
      }, {
        drivetrainId: "0",
        make: "Hyuandai",
        model: "Elantra",
        transTypeId: "0",
        vehicleTypeId: "0",
        vin: "KMHDU46D08U302220",
        year: 2008
      }, {
        drivetrainId: "0",
        make: "Honda",
        model: "Accord",
        transTypeId: "0",
        vehicleTypeId: "0",
        vin: "JHMCG665SXC002892",
        year: 1999
      }, {
        drivetrainId: "1",
        make: "Dodge",
        model: "Grand Caravan",
        transTypeId: "0",
        vehicleTypeId: "2",
        vin: "1B4GP44R6WB767601",
        year: 1998
      }, {
        drivetrainId: "0",
        make: "Volkswagon",
        model: "Beetle",
        transTypeId: "0",
        vehicleTypeId: "0",
        vin: "3vwf17at6em648033",
        year: 2009
      }, {
        drivetrainId: "1",
        make: "Lexus",
        model: "IS250",
        transTypeId: "0",
        vehicleTypeId: "0",
        vin: "jthbf5c24a2096446",
        year: 2010
      }, {
        drivetrainId: "0",
        make: "Honda",
        model: "Civic",
        transTypeId: "1",
        vehicleTypeId: "0",
        vin: "1HGEM219X4L076592",
        year: 2004
      }, {
        drivetrainId: "1",
        make: "Saab",
        model: "9-3",
        transTypeId: "1",
        vehicleTypeId: "0",
        vin: "YS3FA4CY9A1616509",
        year: 2010
      }, {
        drivetrainId: "2",
        make: "Porsche",
        model: "911",
        transTypeId: "1",
        vehicleTypeId: "0",
        vin: "WP0AB2A95DS122080",
        year: 2014
      }, {
        drivetrainId: "2",
        make: "Mazda",
        model: "CX-5",
        transTypeId: "0",
        vehicleTypeId: "2",
        vin: "jm3ke4dy4f0501734",
        year: 2015
      }, {
        drivetrainId: "2",
        make: "",
        model: "",
        transTypeId: "0",
        vehicleTypeId: "2",
        vin: "",
        year: 0
      }, {
        drivetrainId: "2",
        make: "",
        model: "",
        transTypeId: "0",
        vehicleTypeId: "2",
        vin: "",
        year: 0
      }
    ];
    
    return {
      getVehicle: function(vin) {
        return _.find(_vehicles, function(vehicle) {
          return vehicle.vin.toUpperCase() == vin.toUpperCase();
        });
      }
    }
  })

// NEED TO CREATE PROVIDER FOR EACH SERVICE THAT CAN BE CONFIGURED